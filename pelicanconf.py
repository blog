#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = u'Chimrod'
SITENAME = u'Chimrod'
SITEURL = '//localhost:8000'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Blogroll
#LINKS =  (('Pelican', 'http://docs.notmyidea.org/alexis/pelican/'),
#          ('Python.org', 'http://python.org'),
#          ('Jinja2', 'http://jinja.pocoo.org'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('key', '/resources/pubkey.asc')
         ,)

FEED_DOMAIN = SITEURL
FEED_RSS = 'feed'
FEED_ATOM = 'feeds/atom.xml'
FEED_MAX_ITEMS=5
TAG_FEED_RSS = 'feeds/{slug}.xml'
TAG_FEED_ATOM = 'feeds/{slug}.atom.xml'
CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'

DEFAULT_PAGINATION = FEED_MAX_ITEMS

ARTICLE_URL = u'{date:%Y}/{date:%m}/{slug}/'
ARTICLE_SAVE_AS = u'{date:%Y}/{date:%m}/{slug}/index.html'

STATIC_SAVE_AS=u'{path}'

# THEME
THEME= 'theme/bulma/'
PYGMENTS_STYLE = "monokai"
SITELOGO= '/images/profile.webp'
SITETITLE= 'Chimrod'
SITESUBTITLE = 'Des glyphes <span class="amp">&</span> du code'
MAIN_MENU=False
COPYRIGHT_NAME="Sébastien Dailly"
DISABLE_URL_HASH=True



THEME_TEMPLATES_OVERRIDES = \
        [ ]


LINKS= ( ("Git", "http://git.chimrod.com")
       , )
HOME_HIDE_TAGS=True
I18N_TEMPLATES_LANG = "fr_FR"
LOCALE="fr_FR"

TYPOGRIFY = False

STATIC_PATHS = \
    ( 'images'
    , 'extras'
    , 'pages'
    , 'resources'
    )

EXTRA_PATH_METADATA = \
    { 'extras/robots.txt': {'path': 'robots.txt'}
    , 'extras/htaccess':   {'path': '.htaccess' }
    , 'resources/viz.js/worker.js':  {'path': 'resources/viz.js/worker.js'}
    , 'resources/viz.js/convert.js':  {'path': 'resources/viz.js/convert.js'}
    , 'resources/viz.js/full.render.js':  {'path': 'resources/viz.js/full.render.js'}
    , 'resources/viz.js/viz.js':  {'path': 'resources/viz.js/viz.js'}
    , 'resources/viz.js/download.js':  {'path': 'resources/viz.js/download.js'}
    }

PLUGIN_PATHS = ['plugins']


PLUGINS = \
  ( 'related_posts'
  , 'my_typogrify'
  , 'i18n_subsites'
  , 'render_math' )

RELATED_POSTS_IGNORE_TAGS = ["Libre"]

SUMMARY_MAX_LENGTH=100

DOCUTILS_SETTINGS = \
    { "footnote_references":'superscript'
    , 'strip_comments': 'True' }

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
LOCALE = ('fr_FR.utf8', )

DATE_FORMATS = {
    'fr': '%d %b %Y',
}
