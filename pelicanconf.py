#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Wolfgang Wiedmeyer'
SITENAME = u"Wolfi's blog"
SITEURL = ''

PATH = 'content'
THEME = 'pelican-themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'superhero'
CUSTOM_CSS = 'static/custom.css'
FAVICON = 'extras/favicon.ico'
BANNER = 'images/banner.jpg'

# ignore tipuesearch for now as it's not used
IGNORE_FILES = ['.#*', '*~', 'TODO', 'tipuesearch']

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# A list of extra files to copy from the source to the destination
# static paths will be copied under the same name
STATIC_PATHS = [
    'images',
    'videos',
    'extras',
    'docs',
]
EXTRA_PATH_METADATA = {
    'extras/robots.txt': {'path': 'robots.txt'},
    'extras/custom.css': {'path': 'static/custom.css'}
}

# only publish, if status is published
DEFAULT_METADATA = {
    'status': 'draft',
}

SHOW_ARTICLE_AUTHOR = True
SHOW_DATE_MODIFIED = True

PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'
SLUGIFY_SOURCE = 'basename'
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS = (('Home', 'https://fossencdi.org'),
             ('About', './about.html'),
             ('Photos', 'https://fossencdi.org/gallery'),
             ('Research', './research.html'),
             ('Contact', './contact.html'),)

SIDEBARITEMS = (('My setup', './setup.html', 'laptop'),
                ('Notes', './notes.html', 'sticky-note'),)

# HIDE_SIDEBAR = True

# activate if there are more articles and tag page fixed
DISPLAY_TAGS_ON_SIDEBAR = False

# Blogroll
LINKS = (('Debian', 'https://www.debian.org/'),
         ('Replicant', 'https://www.replicant.us/'),
         ('Libreboot', 'https://libreboot.org/'),
         ('F-Droid', 'https://f-droid.org/'),
         ('GNU Octave', 'https://www.gnu.org/software/octave/'),
         ('Fight DRM!', 'https://www.defectivebydesign.org/'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
