title: Guile-2D is now named “Sly”
date: 2014-08-11 19:00
tags: foss, gnu, guile, scheme, gamedev, wsu
---

Guile-2D has been the working title for my game engine written in
Guile Scheme for over a year now.  The name has become limiting since
I realized that it wouldn’t be much extra work to support 3D graphics.
After much indecision, I’ve finally decided on an official name: Sly.
I think it’s a great name.  It’s short, easy to type, and slyness is
one of the definitions of “guile”.

In other news:

* Sly has a new contributor!
  [Jordan Russel](https://gitorious.org/~seconded) has written a
  [new module](https://gitorious.org/sly/sly/source/9231dca37261de8269149ccad4517acad41aa015:sly/joystick.scm)
  that provides joystick input support.

* I have written a
  [module for describing animations](https://gitorious.org/sly/sly/source/9231dca37261de8269149ccad4517acad41aa015:sly/transition.scm).

* I have been slowly working on a scene graph implementation that
  plays well with the functional reactive programming API.

* As mentioned above, 3D graphics support is on the way!  So far, I
  have implemented a perspective projection matrix, a "look at"
  matrix, and a cube primitive.

![3D Scene Graph](/images/sly/scene-graph-prototype.png)

Check out the Sly source code repository on
[Gitorious](https://gitorious.org/sly/sly)!
