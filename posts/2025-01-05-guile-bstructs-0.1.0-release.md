title: Guile-bstructs 0.1.0 released
date: 2025-01-05 18:00:00
tags: guile-bstructs, release
summary: Guile-bstructs 0.1.0 released
---

I'm pleased to announce that the very first release of guile-bstructs,
version 0.1.0, has been released!  This is a library I've been working
on for quite some time and after more than one rewrite and many
smaller refactors I think it's finally ready to release publicly.
Let's hope I'm not wrong about that!

## About guile-bstructs

Guile-bstructs is a library that provides structured read/write access
to binary data for Guile.  A bstruct (short for “binary structure”) is
a data type that encapsulates a bytevector and a byte offset which
interprets that bytevector based on a specified layout.

Some use cases for bstructs are:

* manipulating C structs when using the foreign function interface

* packing GPU vertex buffers when using graphics APIs such as OpenGL

* implementing data types that benefit from Guile's unboxed math
optimizations such as vectors and matrices

This library was initially inspired by guile-opengl's
`define-packed-struct` syntax but is heavily based on ["Ftypes:
Structured foreign
types"](http://scheme2011.ucombinator.org/papers/Keep2011.pdf) by Andy
Keep and R. Kent Dybvig.  The resulting interface is quite similar but
the implementation is completely original.

This library provides a syntax-heavy interface; nearly all of the
public API is syntax.  This is done to ensure that bstruct types are
static and well-known at compile time resulting in efficient bytecode
and minimal runtime overhead.

A subset of the interface deals in raw bytevector access for accessing
structured data in bytevectors directly without going through an
intermediary bstruct wrapper.  This low-level interface is useful for
certain batch processing situations where the overhead of creating
wrapper bstructs would hinder throughput.

## Example

Here is some example code to give you an idea of what it’s like to use
guile-bstructs:

```scheme
;; Struct
(define-bstruct <vec2>
  (struct
   (x float)
   (y float)))

;; Get size/alignment
(bstruct-sizeof <vec2>)  ; => 8
(bstruct-alignof <vec2>) ; => 4

;; Allocation
(define v (bstruct-alloc <vec2> (x 42.0) (y 61.0)))

;; Check type
(bstruct? <vec2> v) ; => #t

;; Read
(bstruct-ref <vec2> v x)   ; => 42.0
(bstruct-ref <vec2> v x y) ; => 42.0, 61.0

;; Write
(bstruct-set! <vec2> v (x 0.0) (y 0.0))

;; Convert to s-expression
(bstruct->sexp <vec2> v) ; => ((struct (x 42.0) (y 61.0)))

;; Convert FFI pointer
(pointer->bstruct <vec2> my-pointer)

;; More type examples:

;; Type group with a union
(define-bstruct
  (<mouse-move-event>
   (struct
    (type uint8)
    (x int32)
    (y int32)))
  (<mouse-button-event>
   (struct
    (type uint8)
    (button uint8)
    (state uint8)
    (x int32)
    (y int32)))
  (<event>
   (union
    (type uint8)
    (mouse-move <mouse-move-event>)
    (mouse-button <mouse-button-event>))))

;; Array
(define-bstruct <matrix4>
  (array 16 float))

;; Bit fields
(define-bstruct <date>
  (bits
   (year 32 s)
   (month 4 u)
   (day 5 u)))

;; Pointer
(define-bstruct
  (<item>
   (struct
    (type int)))
  (<chest>
   (struct
    (opened? uint8)
    (item (* <item>)))))

;; Packed struct modifier
(define-bstruct <enemy>
  (packed
   (struct
    (type uint8)
    (health uint32))))

;; Endianness modifier
(define-bstruct <big-float> (endian big float))

;; Recursive type
(define-bstruct <node>
  (struct
   (item int)
   (next (* <node>))))

;; Mutually recursive type group
(define-bstruct
  (<forest>
   (struct
    (children (* <tree>))))
  (<tree>
   (struct
    (value int)
    (forest (* <forest>))
    (next (* <tree>)))))

;; Opaque type
(define-bstruct SDL_GPUTexture)
```

## Download

Source tarball: [guile-bstructs-0.1.0.tar.gz](https://files.dthompson.us/releases/guile-bstructs/guile-bstructs-0.1.0.tar.gz)

GPG signature: [guile-bstructs-0.1.0.tar.gz.asc](https://files.dthompson.us/releases/guile-bstructs/guile-bstructs-0.1.0.tar.gz.asc)

This release was signed with [this GPG
key](https://files.dthompson.us/releases/dthompson.key).

See the [guile-bstructs project page](/projects/guile-bstructs.html) for more
information.
