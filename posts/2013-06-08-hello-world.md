title: Hello, world!
date: 2013-06-08 17:18
tags: hello
summary: I has a shiny new website
---

Hey! I have a shiny new website. I just purchased a cheap VPS from
[Digital Ocean](http://digitalocean.com) and registered the
[dthompson.us](https://dthompson.us) domain for this site. Instead of
the going the Wordpress route (or using similar blogging software),
I’ve decided to use a static site generator called
[Pelican](http://getpelican.com) after seeing that the
[Linux](http://kernel.org) kernel website was using it. I’m going to
use this site to host this blog, my resume, and information about my
software projects.

A little about myself: I’m a software developer from Massachusetts. I
graduated from [Worcester State University](http://worcester.edu) in
2012 with a BS in Computer Science. I currently work as a junior
[Ruby on Rails](http://rubyonrails.org) developer at
[Vista Higher Learning](http://vistahigherlearning.com). I am an
advocate of free and open source software and I use GNU/Linux as my
operating system of choice. In my spare time, I like to write free
software (mostly my own never-to-be-finished projects). Oddly enough,
I’m still using a proprietary web service to host all of my software
projects, so check out my [Github](https://github.com/davexunit) page.

Connect with me on [Diaspora](https://joindiaspora.com/u/davexunit) or
[Twitter](https://twitter.com/davexunit).
