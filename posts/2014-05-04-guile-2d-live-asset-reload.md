title: Live Asset Reloading with guile-2d
date: 2014-05-04 22:00:00
tags: foss, gnu, guile, scheme, gamedev, wsu, frp
summary: Automatically reload modified assets while developing guile-2d progams
---

Guile-2d provides a dynamic environment in which a developer can build
a game incrementally as it runs via the Guile REPL.  It’s nice to be
able to hot-swap code and have the running game reflect the changes
made, but what about the game data files?  If an image file or other
game asset is modified, it would be nice if the game engine took
notice and reloaded it automatically.  This is what guile-2d’s live
asset reloading feature does.

The new `(2d live-reload)` module provides the `live-reload`
procedure.  `live-reload` takes a procedure like `load-texture` and
returns a new procedure that adds the live reload magic.  The new
procedure returns assets wrapped in a signal, a time-varying value.  A
coroutine is started that periodically checks if the asset file has
been modified, and if so, reloads the asset and propagates it via the
signal.  Game objects that depend on the asset will be regenerated
immediately.

Here’s some example code:

```scheme
(define load-texture/live
  (live-reload load-texture))

(define-signal texture
  (load-texture/live "images/p1_front.png"))

(define-signal sprite
  (signal-map
   (lambda (texture)
     (make-sprite texture
                  #:position (vector2 320 240)))
   texture))
```

`load-texture/live` loads textures and reloads them when they change
on disk.  Every time the texture is reloaded, the sprite is
regenerated using the new texture.

Here’s a screencast to see live reloading in action:

![](/videos/guile-2d-live-reload.webm)

Guile-2d is ever-so-slowly approaching a 0.2 release.  Stay tuned!
