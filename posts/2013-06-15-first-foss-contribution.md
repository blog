title: My First Real FOSS Contribution
date: 2013-06-15 18:00
tags: foss, mediagoblin, python, federated, decentralized, wsu
summary: I added a small feature!
---

I spend a lot of my free time writing code. I usually work on my own
personal projects that never really go anywhere. So, I decided to take
a detour from my normal hacking routine and contribute to an existing
free software project. My contribution was accepted awhile ago now,
but I wasn’t blogging then so I’m rambling about it now.

It’s wise to find a project with a low barrier of entry. An active IRC
channel and/or mailing list with people willing to help newcomers is
ideal. I remembered hearing about
[GNU MediaGoblin](http://mediagoblin.org) at LibrePlanet 2012, so I
decided to check things out. MediaGoblin is a media sharing web
application written in Python. Their bug tracker marks tickets that
require little work and don’t require a deep understanding of
MediaGoblin as bitesized’.

I chose to work on
[this ticket](http://issues.mediagoblin.org/ticket/453) because it
didn’t require any complicated database migrations or knowledge of the
media processing code. I added a new configuration option,
`allow_comments`, and a small amount of code to enforce the setting.

Eventually, the ticket got reviewed and [Christine
Lemmer-Webber](http://dustycloud.org) (MediaGoblin’s friendly project
leader) merged it: “Heya. Great branch, this works perfectly. Merged!”

It was a very small change, but I was happy to _finally_ have some
actual code of mine in a real free software project. I have a strong
passion for free software and the GNU philosophy, so it’s really great
to participate in the community. My job as a professional software
developer eats up a lot of my time these days, but I hope to find the
time to continue hacking and contributing.
