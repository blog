title: How to apply Hilltop's Boku no Natsuyasumi 2 English patch on Linux
date: 2023-11-08 10:00
tags: games, modding
summary: How to apply the English patch for Boku no Natsuyasumi 2 on Linux
slug: boku-no-natsuyasumi-2-english-patch-linux
---

Haven't blogged in awhile, so here’s a quickie.  Last week, Hilltop
[released an English patch for Boku no Natsuyasumi
2](https://www.patreon.com/posts/boku-no-2-patch-92070798), which
finally makes the Japan exclusive PS2 game accessible to us English
speaking folks.  The README explains how to apply the patch on Windows
or via a web utility.  Having never patched an ISO before, I wondered
how it could be done using a native Linux utility instead.  Turns out
it's easy to do via the xdelta tool.

First, get xdelta from your distro’s package repository.  I use Guix
so I ran `guix shell xdelta` rather than install it because this is a
one-off task.

Then patch the ISO:

```
xdelta3 -d -s Boku2.iso Boku2EnglishPatch1.1.xdelta Boku2-patched.iso
```

Did a quick test in my softmodded PS2 with [GrimDoomer’s modified
OPL](https://www.youtube.com/watch?v=nKoRvWKojz0&t=9s&pp=ygUHb3BsIHBzMg%3D%3D)
so I could easily load it from an SSD in the expansion bay and it
works!!

![Boku no Natsuyasumi 2’s title screen showing English text](/images/boku2.jpg)

Of course, I went through all this trouble to play the game because of
[Tim Roger's 6 hour
review](https://www.youtube.com/watch?v=779coR-XPTw&pp=ygUNYWN0aW9uIGJ1dHRvbg%3D%3D)
of the original Boku no Natsuyasumi.  Check it out if you haven’t seen
it.
