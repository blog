title: Spring Lisp Game Jam 2023 Summary
date: 2023-06-05 09:00:00
tags: guile, gamedev, gamejam, chickadee
summary: Check out all of the entries to the Spring Lisp Game Jam!
---

![Super Bloom gameplay GIF](/images/chickadee/super-bloom.gif)

The [Spring Lisp Game Jam
2023](https://itch.io/jam/spring-lisp-game-jam-2023) submission
deadline has now passed and we are now in the rating period.  I was a
co-host of the jam alongside
[technomancy](https://technomancy.itch.io/) but he did all the real
work setting everything up.  There were [30
submissions](https://itch.io/jam/spring-lisp-game-jam-2023/entries)
this time around, which I believe is a new record for our little jam!
This particular jam was a special one for me.  I not only submitted a
game built with [Chickadee](/projects/chickadee.html) but I also got
*paid* to work on another entry as part of my work at the [Spritely
Institute](https://spritely.institute)!

If there’s a downside to doing a game jam as part of your day job,
it’s that there isn’t much time to hack on a game just for funsies.
Woe is me, right?  I *really* wanted to submit something built with
Chickadee, though, not only to get some experience making things with
it but also to validate that the whole stack is actually improving
over time.  So, I did my best to scope my ambitions *way down* and
create the simplest game I could think of that I could still be happy
to share.  That game is [Super
Bloom](https://itch.io/jam/spring-lisp-game-jam-2023/rate/2102759)!
You play as a little bouncing circle, I guess, that is tasked with
helping a young sunflower seedling grow and bloom.  You need to water
it and destroy the dirt balls (which my limited art abilities made
look more like meatballs) that want to kill it for some reason unknown
even to me.  It’s a very minimal game.  Water is used both to grow the
flower and to attack the dirt balls.  The only keys used are the arrow
keys and Z.  Most of the work happened on the weekends at the start
and end of the jam.  I’m quite happy with how it came out given how
little time I spent making it!  I wish I could have added more variety
to the enemy spawn patterns and relied less on random placement, but
the result is OK.

In addition to using Chickadee, this game served as another
opportunity to test drive my still-unreleased game engine named
[Catbird](/catbird-an-experimental-game-engine-for-scheme-programmers.html).
There are still several rough edges but things are definitely headed
in the right direction.  The new in-engine REPL, as crude as it is,
proved very useful for evaluating small expressions quickly without
having to switch back to Emacs.  Much of the game was developed live
at the REPL from within Emacs, while the game was running, except for
when I encountered engine bugs or needed to do a big refactor or
something.  Catbird auto-reloads non-code assets (images, sound, etc.)
so it was quick and easy to check out how sprite updates looked inside
the game.  The biggest change in my tools this time around was using
[Libresprite](https://libresprite.github.io) instead of GIMP for the
pixel art.  I had a great experience with Libresprite and intend to
stick with it for future game dev projects.

For Spritely, Christine and I worked on a distributed, networked
virtual world prototype called
[Fantasary](https://itch.io/jam/spring-lisp-game-jam-2023/rate/2103526).
I spent *a lot* of time on this and even though it doesn’t have much
in the way of gameplay it is a very important prototype for us and if
you want to know more about it then keep an eye on the Spritely blog
because we will be writing about all the details there!  But wait,
there’s more!  Spritely contributed not only one, but *two* entries to
the jam.  Check out
[Wireworld](https://itch.io/jam/spring-lisp-game-jam-2023/rate/2103611)
for the [WASM-4](https://wasm4.org/) fantasy console, made using the
[Guile Hoot](https://gitlab.com/spritely/guile-hoot) WebAssembly
toolchain that Spritely is developing.  There will be more details
about this one on the Spritely blog, as well.

As far as I can tell, there are no other entries that used Guile, but
the Scheme family was well represented (despite what I originally
thought when I first published this) with other entries using Racket,
Chicken, and Kawa.  I haven’t done a real tally, but it seems that
Common Lisp and Fennel (the Lisp that compiles to Lua) were the most
common choices, overall.

Congrats to all jammers that participated, whether you completed your
entry or not.  I’ve been following the Lisp Game Jam for years and it
keeps getting better and better and seeing more and more people catch
the Lisp bug (or curse, depending on your perspective) is exciting!
