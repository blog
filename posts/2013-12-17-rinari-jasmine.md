title: Jump to Jasmine Specs with Rinari
date: 2013-12-17 13:00
tags: emacs, javascript, wsu
summary: I hacked rinari mode to jump between JS sources and jasmine tests
---

I use the [rinari](https://github.com/eschulte/rinari) Emacs mode to
assist me when working on rails projects.  One of rinari’s most useful
features is the ability to quickly jump from one file to another
related file.  I use this feature almost exclusively for jumping
between a ruby class file and its associated rspec file, but lately
I’ve been spending most of my time writing javascript.  At VHL, we use
[jasmine](http://pivotal.github.io/jasmine/) for our unit testing
framework and the
[jasmine ruby gem](https://github.com/pivotal/jasmine-gem) to
integrate it with our rails projects.  Rinari doesn’t have any
built-in jump settings for jasmine test files, so I wrote this quick
hack to make it work:

```elisp
;; Make rinari jump to/from javascript source files and specs.
(setcdr (assoc 'javascript rinari-jump-schema)
        '("j"
          (("app/assets/javascripts/\\1.js" . "spec/javascripts/\\1_spec.js")
           ("spec/javascripts/\\1_spec.js"  . "app/assets/javascripts/\\1.js")
           (t . "spec/javascripts/.*")
           (t . "app/javascripts/.*"))
          t))
(rinari-apply-jump-schema rinari-jump-schema)
```

Now I can press `C-c ; f j` to jump between a javascript file in
`app/assets/javascripts/` and its jasmine test file in
`spec/javascripts/`.  Perhaps I shouldn’t be overwriting the
predefined (but not very useful) javascript jump settings, but I
really wanted to use the `j` key.
