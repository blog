title: The Little Schemer
date: 2013-08-11 15:00:00
tags: scheme, books, wsu
summary: I bought "The Little Schemer"
---

Yesterday, I took a trip to the MIT Press Bookstore and picked up a
copy of
[The Little Schemer](http://mitpress.mit.edu/books/little-schemer). I’ve
only spent a few hours reading and coding along with it, but I’ve had
a lot of fun. The following is a mini-review based on my experience
thus far.

“The Little Schemer” teaches you to think recursively using an
interesting and comedic writing style and the Scheme programming
language. While Scheme is the language of choice, the real goal is to
teach you problem solving rather than the details of a specific
language. The book starts off simple, explaining what atoms, lists,
and S-expressions are. Rather than providing the definition and then
showing examples, it first gives examples in the form of a question
and answer.

Example:

> Is it true that this an atom?
>
> **atom**
>
> Yes, because **atom** is a string of characters beginning with a
> letter.

From the examples given, a definition is created. In later examples, a
Scheme procedure is written that produces the correct answers for all
of the questions stated before it. It’s fun to build the procedure,
verify that it works for all cases, and compare your implementation
with the book’s.

“The Little Schemer” defines ten commandments that are essential to
correctly solving the problems in the book. Some commandments are
first given in an incomplete form, and expanded later when a greater
level of understanding has been achieved. The problems that you solve
reinforce the commandments. You might notice that you start writing
procedures without thinking much about it, much like the muscle memory
earned from using Emacs a lot. Gerald J. Sussman was right when he
said that this book “can perform the same service that Hanon’s finger
exercises or Czerny’s piano studies perform for the student of the
piano.” I have no idea who Hanon and Czerny are, but I get it. For the
drummers out there, you could liken this book to
[Stick Control](http://www.amazon.com/Stick-Control-For-Snare-Drummer/dp/1892764040).

The writing style is very informal, comedic, and food themed. Page 13
has a space reserved for jelly stains, and page 52 tells you to “go
cons a piece of cake onto your mouth.” I have laughed a number of
times while reading. Oh, and let’s not forget about the cute elephant
drawings. This is definitely not your average boring, dry computer
science book. If you are interested in a unique and enjoyable learning
experience, then I highly recommend reading “The Little Schemer”.
