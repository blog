title: Guile-syntax-highlight 0.2.0 released
date: 2022-12-11 20:00:00
tags: guile, release
summary: Guile-syntax-highlight 0.2.0 has been released
---

I'm happy to announce that Guile-syntax-highlight 0.2.0 has been
released!  This is a pretty slow moving project but the last (and
only) release was in 2018 so this release is long overdue.

Guile-syntax-highlight is a general-purpose syntax highlighting
library for GNU Guile. It can parse code written in various
programming languages into a simple s-expression that can be easily
converted to HTML (via SXML) or any other format for rendering.

Notable changes:

* New Common Lisp highlighter.
* New CSS highlighter.
* New Git ignore file highlighter.
* New `lex-consume-until` procedure.

Thanks to Julien Lepiller and Filip Lajszczak for their contributions
to this release!

source tarball: <https://files.dthompson.us/guile-syntax-highlight/guile-syntax-highlight-0.2.0.tar.gz>

signature: <https://files.dthompson.us/guile-syntax-highlight/guile-syntax-highlight-0.2.0.tar.gz.asc>

See the [Guile-syntax-highlight project
page](/projects/guile-syntax-highlight.html) for more information.

Bug reports, bug fixes, feature requests, and patches are welcomed.
