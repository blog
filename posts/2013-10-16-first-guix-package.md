title: My First GNU Guix Patch
date: 2013-10-16 21:00
tags: gnu, guix, scheme, guile, wsu
summary: I packaged libtheora for GNU Guix
---

Over the weekend, I decided to try out GNU Guix: A fully functional
package manager based on Nix and a distribution of the GNU system. I’m
a big proponent of GNU Guile, thus I was excited to see a DSL for
package management written with Guile.

I was told that libtheora would be pretty easy to package, and it
was. Here’s what the package definition looks like:

```scheme
(define libtheora
  (package
    (name "libtheora")
    (version "1.1.1")
    (source (origin
             (method url-fetch)
             (uri (string-append "http://downloads.xiph.org/releases/theora/libtheora-"
                                 version ".tar.xz"))
             (sha256
              (base32
               "0q8wark9ribij57dciym5vdikg2464p8q2mgqvfb78ksjh4s8vgk"))))
    (build-system gnu-build-system)
    (inputs `(("libvorbis" ,libvorbis)))
    ;; The .pc files refer to libogg.
    (propagated-inputs `(("libogg" ,libogg)))
    (synopsis "Library implementing the Theora video format")
    (description
     "The libtheora library implements the ogg theora video format,
a fully open, non-proprietary, patent-and-royalty-free, general-purpose
compressed video format.")
    (license license:bsd-3)
    (home-page "http://xiph.org/theora/")))
```

Pretty slick, eh? Now, I’m starting to work on packaging SDL (1.2
and 2) and the SDL extensions (gfx, ttf, etc.), which are not quite as
easy. I hope to package all of the dependencies that guile-2d will
need to be available as a Guix package.
