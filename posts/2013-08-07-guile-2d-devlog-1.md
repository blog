title: guile-2d - A 2D Game Development Framework for GNU Guile
date: 2013-08-07 23:00:00
tags: foss, gnu, guile, scheme, gamedev, wsu
summary: I have started work on a 2D game framework
---

This is the very first devlog entry for my pet project, guile-2d. As
the title suggests, guile-2d is a 2D game development framework for
[GNU Guile](https://gnu.org/s/guile), a Scheme implementation that has
the honor of being the official extension language of the GNU
project. Guile is a language with a growing number of features, but it
still lacks a large assortment of libraries. I like to do 2D game
programming, and I saw a niche that needed to be filled. Python has
Pygame, Lua has Love, but there’s no fun and accessible game
programming library for Guile. Guile-2d is working to correct that.

The goal of Guile-2d is to create an easy to use 2D game programming
framework. Libraries like SDL give the programmer a rather low-level
set of tools that they can use to build a game, guile-2d will provide
high-level tools over low-level SDL and OpenGL for commonly used
elements of 2D games: tile maps, sprite animation, particle systems,
collision detection, vector math, A* pathfinding, etc. Such features
will allow a game developer to very quickly produce a working
prototype with guile-2d.

Guile-2d is a framework, which means that it has some opinion about
what the right way to do things is. The most apparent example of this
is the game loop. The game loop runs at 60 frames-per-second and uses
fixed timestep updates. Those that have read
[Fix Your Timestep](http://gafferongames.com/game-physics/fix-your-timestep/)
will know that this decision is a good thing.

Perhaps the most important feature of guile-2d is the ability to do
“live coding”. When the game loop starts, a REPL
(read-eval-print-loop) server is started. Using the great
[Geiser](http://www.nongnu.org/geiser/) extension for Emacs to connect
to the REPL server, one can modify their game as it is running. This
gives users the power to evaluate some new code and see the changes
reflected immediately in the game window. No need to restart the game
unless you crash it!

This has been a brief overview of some of the features and goals of
guile-2d. If this project interests you, you can check out the source
code on [Github](https://github.com/davexunit/guile-2d).
