title: Guile-websocket 0.2.0 released
date: 2025-01-23 12:00:00
tags: guile, release
summary: Guile-websocket 0.2.0 has been released
---

I'm happy to announce that guile-websocket 0.2.0 has been released!

Guile-websocket is an implementation of the [WebSocket
protocol](https://datatracker.ietf.org/doc/html/rfc6455), both the
client and server sides, for Guile Scheme.

This release introduces breaking changes that overhaul the client and
server implementations in order to support [non-blocking
I/O](https://www.gnu.org/software/guile/manual/html_node/Non_002dBlocking-I_002fO.html)
and TLS encrypted connections.

source tarball: <https://files.dthompson.us/guile-websocket/guile-websocket-0.2.0.tar.gz>

signature: <https://files.dthompson.us/guile-websocket/guile-websocket-0.2.0.tar.gz.asc>

See the [guile-websocket project page](/projects/guile-websocket.html)
for more information.

Bug reports, bug fixes, feature requests, and patches are welcomed.
