title: Guile-Websocket 0.1 released
date: 2022-11-12 21:00:00
tags: guile, release
summary: Guile-Websocket 0.1 has been released
---

I'm happy to announce that Guile-Websocket 0.1 has been released!

Guile-Websocket is an implementation of the WebSocket protocol, both
the client and server sides, for Guile Scheme.

source tarball: <https://files.dthompson.us/guile-websocket/guile-websocket-0.1.tar.gz>

signature: <https://files.dthompson.us/guile-websocket/guile-websocket-0.1.tar.gz.asc>

See the [Guile-Websocket project page](/projects/guile-websocket.html)
for more information.

Bug reports, bug fixes, feature requests, and patches are welcomed.
