title: StumpWM on Debian Wheezy
date: 2013-07-20 15:00
tags: stumpwm, common lisp, debian, wheezy, wsu
summary: First steps with StumpWM on Debian Wheezy
---

Everyone that’s ever talked to me about software development knows
that I am in love with Emacs. Emacs has a wonderful keyboard driven
interface and is almost infinitely customizable via Emacs Lisp. I’ve
done a lot of programming in Emacs from my not-so-great laptop
lately. My laptop has a rather poor 1280x800 resolution and low
performing integrated graphics chip. Until today, I was running the
GNOME 3 desktop environment on it. Unlike most people (or perhaps just
a loud minority), I like GNOME 3. However, I wanted something that was
both less graphics intensive and more keyboard driven than GNOME
Shell and Mutter.

Someone on IRC told me about
[StumpWM](http://www.nongnu.org/stumpwm/), a window manager written
entirely in Common Lisp. I had heard of StumpWM before, but back then
I wasn’t an Emacs user and I’ve never really stuck with any tiling
window manager that I’ve tried (DWM, Awesome). Now that I know the
power of a fully programmable environment thanks to Emacs, I decided
to give StumpWM a try. After some initial pains trying to get it to
run, I am now using it very happily.

Here is what I had to do to get StumpWM running on Debian Wheezy.

1) Install StumpWM

```sh
sudo apt-get install stumpwm
```

2) Create an `.xinitrc` file in my home directory with the following
   text

```sh
exec stumpwm
```

3) Workaround clisp “component not found” errors

I could not get StumpWM to start until I created the following
symbolic links:

```sh
ln -s /usr/share/common-lisp/source/stumpwm/stumpwm.asd /usr/share/common-lisp/systems/stumpwm.asd
ln -s /usr/share/common-lisp/source/cl-ppcre/cl-ppcre.asd /usr/share/common-lisp/systems/cl-ppcre.asd
```

4) Start the X server

```sh
startx
```

I use the GNOME Desktop Manager, so I also created a session file for
StumpWM in `/usr/share/xsessions/stumpwm.desktop`.

```
[Desktop Entry]
Encoding=UTF-8
Name=StumpWM
Comment=This session logs you into StumpWM
Exec=stumpwm
TryExec=stumpwm
Icon=
Type=Application
```

I hope this brief installation guide can be of use to one of you out
there in Internet land. Perhaps in the future I will write an article
about customizing StumpWM with Common Lisp.
