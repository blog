��          �   %   �      p     q     �     �     �     �  3   �  
   �     �            .        M  	   Y     c  *   o  "   �     �     �     �     �     �  	   �  B        D     Q     ]     b  �  r     f     �     �     �     �  ?   �     �               (  4   0     e     t     �  :   �  $   �     �            	     
         +  E   9          �     �     �                                                  
                    	                                                     %(minutes)s min read Archives Atom Authors Built with %(pelican_url)s Built with %(pelican_url)s using %(flex_url)s theme Categories Category %(name)s Continue reading Home Like this article? Share it with your friends! Newer Posts Next Post Older Posts Please enable JavaScript to view comments. Posted on %(when)s in %(category)s Posts by %(name)s Previous Post RSS Search Search Results Search... Switch to the %(dark_url)s | %(light_url)s | %(browser_url)s theme Tag %(name)s Tagged with Tags You might enjoy Project-Id-Version: Flex 2.1.0
Report-Msgid-Bugs-To: EMAIL@ADDRESS
PO-Revision-Date: 2024-04-03 18:44+0200
Last-Translator: Charles Brunet <charles@cbrunet.net>, 2021
Language-Team: French (https://www.transifex.com/alexandrevicenzi/teams/66327/fr/)
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
Generated-By: Babel 2.8.0
X-Generator: Poedit 3.2.2
 lecture de %(minutes)s min Archives Atom Auteurs Généré avec %(pelican_url)s Construit avec %(pelican_url)s utilisant le thème %(flex_url)s Catégories Catégorie %(name)s Continuer à lire Accueil Vous aimez cet article ? Partagez le avec vos amis ! Nouveaux Posts Post suivant Anciens Posts Veuillez activer le JavaScript pour voir les commentaires. Posté le %(when)s dans %(category)s Posts de %(name)s Post précédent RSS Recherche Résultats Rechercher… Changer pour le thème %(dark_url)s | %(light_url)s | %(browser_url)s Tag %(name)s Taggé avec Tags Vous pourriez aimer 