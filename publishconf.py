#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append('.')
from pelicanconf import *

SITEURL = '//blog.chimrod.com'
FEED_DOMAIN = SITEURL

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

# Uncomment following line for absolute URLs in production:
#RELATIVE_URLS = False

#DISQUS_SITENAME='chimrod'
#GOOGLE_ANALYTICS = ""

import sys
import os.path
try:
    sys.path.append(os.path.dirname(os.path.abspath(__file__)))
    from disqus import *
    PLUGINS.append("disqus_static")
except:
    pass

