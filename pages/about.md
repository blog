title: About me
---

## Hi.

I am a professional software developer.  I like to write
programs in Scheme in my free time, such as the software that built
this website.

I also like to garden and make things out of wood.

If you're into social media, you can follow me on
[Mastodon](https://toot.cat/@dthompson).
