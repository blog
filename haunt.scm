;;; Copyright © 2018-2021 David Thompson <davet@gnu.org>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt builder flat-pages)
             (haunt builder redirects)
             (haunt post)
             (haunt publisher rsync)
             (haunt site)
             (markdown)
             (projects)
             ((redirects) #:prefix dthompson:)
             (theme)
             (utils))

(define post-prefix "/posts")

(define collections
  `(("Recent Blog Posts" "index.html" ,posts/reverse-chronological)))

(site #:title "dthompson"
      #:domain "dthompson.us"
      #:default-metadata
      '((author . "David Thompson")
        (email  . "davet@gnu.org"))
      #:readers (list commonmark-reader*)
      #:builders (list (blog #:theme dthompson-theme
                             #:collections collections
                             #:post-prefix post-prefix
                             #:posts-per-page 10)
                       (redirects dthompson:redirects)
                       (atom-feed #:blog-prefix post-prefix)
                       (atom-feeds-by-tag #:blog-prefix post-prefix)
                       (flat-pages "pages"
                                   #:template (theme-layout dthompson-theme))
                       chickadee-page
                       sly-page
                       guile-sdl2-page
                       guile-syntax-highlight-page
                       guile-websocket-page
                       guile-bstructs-page
                       haunt-page
                       shroud-page
                       srt2vtt-page
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "videos")
                       (static-directory "src")
                       (static-directory "embeds"))
      #:publishers (list (rsync-publisher #:destination "/var/www/blog"
                                          #:user "publish"
                                          #:host "dthompson.us"))
      #:make-slug post-slug-v2)
