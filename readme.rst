
Ce dépôt contient le blog hébergé à l'adresse suivante : https://blog.chimrod.com/

Installation
============

Dépendances
-----------

Installer pélican dans un environnement virtuel :

.. code-block:: bash

  mkvirtualenv pelican
  workon pelican
  pip3 install pelican smartypants pygments typogrify

Copie du dépôt
--------------

.. code-block:: bash

  git clone --recursive https://git.chimrod.com/blog.git/


Mise à jour
===========

Lors de la mise à jour, pour obtenir également la mise à jour des sous-modules
associés :

.. code-block:: bash

  git pull --recurse-submodules
  pip3 install --upgrade pelican smartypants pygments typogrify


Génération et copie
===================

La commande :literal:`make sync` génère toutes les pages, et les transfert en ligne.
