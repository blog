.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Fusion de CSS
=============

:status: hidden
:save_as: pages/css_merger/index.html
:url: pages/css_merger/
:tags: application
:date: 2021-02
:summary:

  Outil de fusion de CSS en ligne. Je l’utilise dans ce blog pour surcharger le
  thème par défaut (dans une version exécutable).


Chargez les fichiers à fusionner :

.. raw:: html

  <noscript>Sorry, you need to enable JavaScript to see this page.</noscript>
    <script id="css_js" type="text/javascript" defer="defer" src="{static}css.js"></script>
  <script>
    var script = document.getElementById('css_js');
    script.addEventListener('load', function() {
      var div = document.getElementById('merger');
      merger.attach(div);
    });
  </script>
  <div id="merger"></div>

Le principe
-----------

Les fichiers CSS_ sont définis par un ensemble de règles qui s'appliquent sur
des éléments de la page. Ces règles peuvent être redéfinies dans le même
fichier, ou même dans d'autres fichiers qui sont chargés après le premier (la
dernière règle chargée prévaut).

.. _css: https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade

Afin d'optimiser le chargement des pages web, il est souvent conseiller de
fusionner les fichiers CSS en un seul (moins de bande passante consommée, un
seul transfert réseau, temps de chargement des règles inutiles évité). Cela est
possible en appliquant les règles successivement en analysant les fichiers.

Utilisation
-----------

Chargez juste les fichiers, le résultat du traitement est affiché dans la
fenêtre de prévisualisation. Il est également possible de télécharger la version
du fichier réécrite pour en minimiser la taille.

Code source
-----------

Le code est disponible sur mon `dépôt git`_. Il utilise une version
compilée en JavaScript de l'application `css_merger` qui s'y trouve.

.. _dépôt git: http://git.chimrod.com/css_lib.git/
