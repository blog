.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Ardoise calligraphique
======================

:status: hidden
:save_as: pages/script.it/index.html
:url: pages/script.it/
:tags: application
:date: 2021-01
:summary:

  Application javascript pour dessiner dans le navigateur avec le même rendu
  qu'une plume.

  .. image:: {static}/pages/applications/calligraphie.svg
   :align: center
   :target: {filename}/pages/script.it/script.rst#ardoise-calligraphique


.. raw:: html

  <noscript>Sorry, you need to enable JavaScript to see this page.</noscript>
    <script id="drawer_js" type="text/javascript" defer="defer" src="{static}script.js"></script>
  <script>
    var script = document.getElementById('drawer_js');
    script.addEventListener('load', function() {
      var app = document.getElementById('slate');
      drawer.run(app);
    });
  </script>
  <section class="todoapp" id="app">
      <canvas id="slate" class="drawing-zone" width="800" height="800">
  </section>

Cliquez dans l'ardoise pour commencer à dessiner !
