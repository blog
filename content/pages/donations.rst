:title: Donations
:status: published

If you like my free software work or the web services I provide, you can help me with a donation. Donations allow me to sustain my development work and pay server costs.

You can donate to me in the following ways:

- Bitcoin: 1FMtGbWEpHntUe1XnMhtDTKkanEj2jrbXW or `click here <./images/bitcoinQR.png>`__ for the QR code.

- Hardware donations or wire transfer: Please `contact me <{filename}/pages/contact.rst>`__ for the details.

- Cash or check: `See here <{filename}/pages/impressum.rst>`__ for the address.

Thank you very much! ♥
