:title: Contact
:status: published

Feel free to send an email to wreg@wiedmeyer.de. I'd be happy to get comments, corrections or suggestions.

| Please use GPG encryption, so you won't send out digital postcards all the time. My GPG key can be found `here <https://wiedmeyer.de/keys/ww.asc>`__ or you can get it from a keyserver such as hkps.pool.sks-keyservers.net. The key should have the following fingerprint:
| 0F30 D1A0 2F73 F70A 6FEE  048E 5816 A24C 1075 7FC4
| If you are new to mail encryption: `This <https://emailselfdefense.fsf.org>`__ is a very good introductory guide. If you want to go a step further and follow best practices, have a look `here <https://riseup.net/en/security/message-security/openpgp/best-practices>`__.

I'm also available via Jabber/XMPP upon request. You may additionally find me on a few of the popular IRC networks. My nick is wiewo.
