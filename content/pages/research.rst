:title: Research and Education
:status: published

| A short form of my CV in `English <./docs/cv_eng.pdf>`__ and `German <./docs/cv_ger.pdf>`__
| Feel free to contact me for a long form.
|

The main focus of my engineering studies was medical technology and robotics. Besides the classic engineering basics, I dived into the fields of automation, mechatronics, sofware engineering and simulation. I have a special interest in humanoid robots, biomimetics and prosthetics.

Theses
######

Currently, only my Facharbeit and bachelor's thesis are available and both are written in German. I'm planning to add the remaining theses I wrote during my engineering studies.

`Pax Augusta – Pax Americana <./docs/Facharbeit.pdf>`__
-------------------------------------------------------
| My Facharbeit. A short summary in German:
| Die Arbeit versucht einen Vergleich zwischen Rom zur Zeit von Augustus und Amerika nach dem 2. Weltkrieg zu ziehen. Vor allem das Friedensverständnis und die Sicherung der Vormachtstellung beider Großmächte wird untersucht. Gemeinsamkeiten und Unterschiede in der Motivation, Politik, Kultur und den geopolitischen Gegebenheiten werden aufgezeigt. Abschließend wird ein Ausblick auf die zukünftigen Herausforderungen zur Erlangung eines universalen Friedens gewagt. 

`Dynamische Simulation eines biartikulären Antriebssystems mit variabler Steifigkeit <./docs/bachelors_thesis.pdf>`__
---------------------------------------------------------------------------------------------------------------------
My bachelor's thesis. The thesis introduces a variable stiffness actuator with biarticular coupling inspired
by the human muscular system, which neither requires artificial pneumatic muscles
nor tendon coupling. When presenting the results of a literature research focused on
the properties of biarticularity of human muscles, the emphasis is on the advantages
of energy transfer between joints and the state of the art of biarticular actuators in
robotics is shortly discussed. Then the presented actuator is modeled and a cyclic
movement is simulated in order to determine the amount of work at the joints. The
results are compared to an actuator without biarticular coupling.

.. introduces a variable stiffness actuator with biarticular coupling inspired by the human muscular system. The actuator is modeled and energy transfer between the joints is investigated. The results are compared to an actuator without biarticular coupling.
