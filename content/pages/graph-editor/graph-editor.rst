
Graphviz en ligne
=================

:status: hidden
:save_as: pages/graph-editor/index.html
:url: pages/graph-editor/
:tags: application
:date: 2020-12
:summary:

  Application javascript pour représenter des graphes

  .. image:: {static}/resources/viz.js/example.svg
   :align: center
   :target: {filename}/pages/graph-editor/graph-editor.rst#graph-editor


.. raw:: html

 <style>

 #app {
   width: 100%;
   height: 100%;
   overflow: hidden;
 }

 #panes {
   display: flex;
   width: 100%;
 }

 #graph {
   flex: 1;
   width: 50%;
 }


 #editor {
   border-right: 1px solid #ccc;
   flex: 1;
 }

 #output svg { top: 0; left: 0; width: 100%; height: 100%; }

 </style>

 <div id="app">
         <button id="dot_output">Export DOT</button>
         <button id="png_output">Export PNG</button>
         <button id="btn_window">Isoler</button>
   <div id="panes" class="split split-horizontal">
     <textarea id="editor">g1
  Nom
  -
  propriété 1 <-> g2:2
  propriété 2

 g2
  Autre
  -
  propriété 1
  encore une autre <i>entrée</i>
  autre champ
    -> g1:3 retour
    -> g3:1

 g3
  Dernier élément
  -
  ligne 2
  ligne 3</textarea>
        <div id="graph" class="split">
          <div id="output">
            <div id="svg"></div>
          </div>
        </div>
      </div>
    </div>

    <script src="{static}/resources/viz.js/convert.js"></script>
