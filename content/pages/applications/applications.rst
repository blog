.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Applications
============

:template: applications
:save_as: pages/applications/index.html
:url: pages/applications/

.. note::

    Voir la charte_ que je m’engage de suivre quant aux applications présentes
    sur ce site.

.. _charte: {filename}/pages/charte.rst#applications-en-ligne

