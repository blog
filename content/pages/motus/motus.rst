.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Solveur pour SUTOM
==================

:status: hidden
:save_as: pages/motus/index.html
:url: pages/motus/
:tags: application
:date: 2022-01
:summary:

  .. class::
   :center:

   .. line-block::

     🟥🟦🟦🟡🟡🟡🟦
     🟥🟥🟥🟥🟥🟥🟥

.. raw:: html

  <style>

  * {
    box-sizing: border-box;
  }

  /* Column container */
  article > div {
    display: flex;
    flex-wrap: wrap;
  }

  /* Side column */
  #regles-actives {
    flex: 30%;
    padding: 20px;
  }

  /* Main column */
  #analyse {
    flex: 70%;
    padding: 20px;
  }

  input[type='text'] { font-size: 24px; }

  fieldset {
    display: table;
    margin: 2em 0;
  }
  fieldset div {
    display: table-row;
  }

  fieldset label {
   display: table-cell;
   padding-right: 20px;
   vertical-align: middle;
  }

  #table { width: unset; }
  table tr td input { width: 2em; text-align: center}
  #table tr td input.wellplaced { background-color: lightgreen;}
  #table tr td input.misplaced { background-color: gold;}
  #table tr:first-child { outline: thin solid; }

  </style>
  <noscript>Sorry, you need to enable JavaScript to see this page.</noscript>
    <script id="lib" type="text/javascript" defer="defer" src="{static}motus.js"></script>
  <script>
    var script = document.getElementById('lib');
    script.addEventListener('load', function() {
    lib.run("length"
           , "send"
           , "dictonnary"
           , "proposition"
           , "side_list"
           , "table"
           , "next_btn"
           , "reload");
    });
  </script>

Analyse
-------

.. raw:: html

    <button  style="float:right;" id="reload"><i class="fa fa-redo-alt"></i></button>
    <table id="table" style="margin: 0 auto">
    </table>
    <button id="next_btn" hidden="true">Suivant</button>
  <fieldset>
    <div>
      <label for="source">Lettres</label>
      <input type="number" id="length" name="source" value="7"/>
    </div>
    <div>
      <label>Proposition initiale</label>
      <div id="proposition"/></div>
    </div>
    <div>
      <label for="dictonnary">Dictionnaire</label>
      <select id="dictonnary">
        <option value="english">Anglais / Americain (61 580 mots)</option>
        <option selected value="french">Français (240 135 mots)</option>
      </select>
    </div>
    <input id="send" type="submit" value="Charger"/>
  </fieldset>

Explications
~~~~~~~~~~~~

Ce programme permet de résoudre les grilles de lettres dans lesquelles il faut
deviner grace à des informations indiquant si le placement des lettres est
juste ou non.

Une version du jeu en ligne est disponible sous le nom du SUTOM_ (en
français) ou WORDLE_ (en anglais).

Le code présenté ici s’appuie sur le principe des arbres de décisions pour
identifier le mot le plus pertinent à présenter à chaque coup. Une version en
ligne de commande est présente sur cette page_.

Après avoir chargé le dictionnaire, vous pouvez cliquer sur les lettres pour
indiquer si celle-ci sont correctement placées ou non :

- lettre en vert : celle-ci est à la bonne place
- lettre en jaune : celle-ci est présente dans le mot, mais à une place
  différente

.. _SUTOM: https://sutom.nocle.fr/
.. _WORDLE: https://www.powerlanguage.co.uk/wordle/
.. _page: https://gist.github.com/Chimrod/575a2fe70e756c1f731fac6404320249

Règles actives
--------------

.. raw:: html

    <ul id="side_list" />

