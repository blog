:status: published
:slug: impressum

Impressum und ViSdP
###################

Wolfgang Wiedmeyer

Wolke 7

00000 Himmel

E-Mail: wreg@wiedmeyer.de

E-Mails bitte mit meinem PGP-Schlüssel verschlüsseln. Der Schlüssel kann `hier <https://wiedmeyer.de/keys/ww.asc>`_ oder von einem Keyserver wie hkps.pool.sks-keyservers.net heruntergeladen werden. Mehr dazu unter `Contact <./contact.html>`_.


Datenschutzerklärung
####################

Der Webserver speichert in Logdateien die IP-Adressen der Rechner, die auf die Website zugreifen. Falls sie in den Anfragen ebenfalls übertragen werden, werden auch `User Agent <https://de.wikipedia.org/wiki/User_Agent>`_ und `Referrer <https://de.wikipedia.org/wiki/Referrer>`_ erfasst. Die erhobenen Daten dienen der Fehlersuche im Krisenfall und werden ansonsten nicht ausgewertet. Es ist mir nicht möglich, die erhobenen Daten einer bestimmten Person zuzuordnen. Weitere Daten werden nicht erhoben, gespeichert oder ausgewertet. Insbesondere werden keine `Cookies <https://de.wikipedia.org/wiki/HTTP-Cookie>`_, kein `Canvas Fingerprinting <https://de.wikipedia.org/wiki/Canvas_Fingerprinting>`_ oder andere Techniken genutzt, die es ermöglichen würden, Nutzer zu identifizieren.
