============
Page de démo
============

:status: hidden

.. contents::

Contenu de texte
================

*Lorem ipsum dolor sit amet, consectetur adipiscing elit*, sed do eiusmod tempor
incididunt ut labore & dolore magna aliqua. Faucibus ornare suspendisse sed
nisi lacus sed viverra. Vestibulum lectus mauris ultrices eros in. Sed lectus
vestibulum mattis ullamcorper velit sed ullamcorper morbi. **Arcu felis bibendum
ut tristique** & egestas quis ipsum. Id volutpat lacus laoreet non curabitur
gravida arcu ac.

..

    Urna et pharetra pharetra massa massa ultricies mi quis.

Ac odio tempor orci dapibus. Consectetur lorem donec massa sapien faucibus &
molestie ac feugiat. Laoreet suspendisse interdum consectetur libero id
faucibus nisl. A scelerisque purus semper eget duis. Ornare arcu dui vivamus
arcu felis bibendum. Sed adipiscing diam donec adipiscing tristique risus nec
feugiat. ``Orci dapibus ultrices in iaculis nunc sed``. Orci sagittis eu volutpat
odio facilisis mauris sit amet massa. Sed risus ultricies tristique nulla
aliquet enim tortor at auctor.

.. math::

  α_t(i) = P(O_1, O_2, … O_t, q_t = S_i λ)

.. math::

  \text{Gain}(S, A) = \text{Entropie}(S) - \sum_{v \in \text{valeurs}(A)}
    \frac{|S_v|}{|S|} \text{Entropie}(S_v)



.. Previous text

.. |replace| replace:: replace

Code
====

.. include:: demo.rst
 :code: rst
 :number-lines: 1
 :start-after: Previous text
 :end-before: :end-before

Tables
======

Avec en-tetes
-------------

.. csv-table:: Frozen Delights!
   :header: "Treat", "Quantity", "Description"
   :widths: 15, 10, 30

   "Albatross", 2.99, "On a stick!"
   "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
   crunchy, now would it?"
   "Gannet Ripple", 1.99, "On a stick!"

Et sans
-------

.. csv-table::
   :widths: 15, 10, 30

   "Albatross", 2.99, "On a stick!"
   "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
   crunchy, now would it?"
   "Gannet Ripple", 1.99, "On a stick!"

Listes
======

1.  Premier élément de la liste
2.  second élément de la liste
3.  dernier élément de la liste

*   Premier élément de la liste
*   second élément de la liste
*   dernier élément de la liste

-----

Définition
    Paragraphe associé à la définition

    2nd Paragraphe

Définition2
    Que se passe-t-il ?

    \


Admonitions
===========

.. _`Lien`: #Admonitions

.. admonition:: Default

  Ce texte s’affiche dans les couleurs par défaut

  `Lien`_


.. admonition:: Danger
  :class: danger

  Ce texte s’affiche avec le style danger

  `Lien`_

.. admonition:: Attention
  :class: warning

  Ce texte s’affiche avec le style attention

  `Lien`_

.. admonition:: Hint
  :class: hint

  Ce texte s’affiche avec le style hint

  `Lien`_

.. note::

  Une note

  `Lien`_

Images
======

.. image:: /images/profile.webp
  :align: center
  :alt: La photo du profil

----------

.. figure:: /images/profile.webp
  :alt: La photo du profil
  :align: left

  Left

.. figure:: /images/profile.webp
  :alt: La photo du profil
  :align: center

  Center

.. figure:: /images/profile.webp
  :alt: La photo du profil
  :align: right

  Right
