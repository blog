.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

======
Charte
======

Ce blog est intégralement en français, dans lequel je partage les projets
que je peux réaliser. Il est hébergé sur une plate-forme indépendante, et je
m’efforce de respecter l’utilisateur qui vient le consulter :

Vie privée
==========

Collecte d’informations
-----------------------

Le site ne contient aucun service tiers (commentaire, publicitaire etc) qui
serait imposé au visiteur. Il n’y a par conséquent aucun moyen de me laisser de
commentaire sur les articles, mais je suis joignable sur différents canaux
(`journal du hacker`_, reddit_, …) qui permettent de laisser des messages
privés.

.. _`journal du hacker`: https://www.journalduhacker.net/u/Chimrod
.. _`reddit`: https://www.reddit.com/user/Chimrod

Resources
---------

Les resources (images, codes, documents) sont toutes hébergées sur le site, mais
le chargement des polices entraine la sollicitation des ressources de Google
(via `google font`_)

.. _`google font`: https://fonts.google.com/

Applications en ligne
=====================

Des petits projets en ligne sont présents dans la page Applications_.  Il
s'agit d'outil dont je me sers au quotidien ou des petits projets que j'ai
construits.  Ils sont codés en OCaml, et présentent toutes les caractéristques
suivantes :

.. _Applications: {filename}/pages/applications/applications.rst

Tout est exécuté dans le navigateur
-----------------------------------

Je n’ai donc aucun moyen de savoir ce que vous pouvez faire avec, les
données étant uniquement traitées sur votre ordinateur.

Rien n’est transmis sur le réseau
---------------------------------

Il n’y a pas d’appel à des services externes, et il n’est pas possible de
« sniffer » votre activité. Si des ressources nécessaires au fonctionnement
sont chargées, elle le sont à partir du site du blog et ne dépendent pas d’un
hébergeur tiers.

Le code est disponible
----------------------

L’ensemble du code de ces programmes est en ligne sur `mon dépot`_. Vous pouvez
donc inspecter le code, le réutiliser, ou le modifier, *sans être tenu de m’en
informer*.

.. _mon dépot: https://git.chimrod.com/blog_scripts.git/
