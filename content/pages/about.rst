:title: About
:status: published

.. figure:: |filename|/images/self.jpg
   :figwidth: 180px
   :alt: image of myself
   :align: left

I'm 27 years old and live near Munich, Germany. During the last years, I was
busy studying mechanical engineering with the main focus on robotics and
medical engineering. Now I'm working at the `Intelligent Process Automation and Robotics Lab <http://www.ipr.kit.edu/english/index.php>`_ at `KIT <http://www.kit.edu/english/>`_.

I'm a free software advocate and I'm convinced that the freedom of our
computing is an essential part of a free society. I also just very much enjoy
using free software and working with others to improve it. I have an interest in
encryption and privacy/security issues. Additionally, I think that the
internet should be decentralized to ensure control over our own data.
In order to improve the situation, I am trying to host all the services I need on my
own servers and I also provide them to my family and friends.

Photography and filming is another passion of mine, including the post-processing
part. That's why I'm part of the `Munich Filmworks project <https://munich-filmworks.com/>`_.
I like to do sports, too. Nowadays, I am just trying to stay fit, but I was an
avid 400m sprinter for some years and competed in various track and field events.  

Besides all of this, I like to watch good or silly movies, preferably in English
to improve my skills in this language.
In the past, I was reading a lot of books and every National Geographic issue from
the first to the last page. These days, due to the lack of time and because National
Geographic is now part of the Murdoch empire, I've mainly resorted to keeping up with
current news, reading political commentary and following various blogs.
