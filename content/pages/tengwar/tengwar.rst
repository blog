.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==================
Écrire les tengwar
==================

.. role:: tengwar

:status: hidden
:save_as: pages/tengwar/index.html
:url: pages/tengwar/
:tags: application
:date: 2021-09
:summary:

  .. raw:: html

    <style>
    @font-face {
      font-family: "Tengwar Annatar" ;
      src: url("/pages/tengwar/tngan.ttf") format("truetype");
    }

    .tengwar {
      font-family: "Tengwar Annatar" ;
    }
    </style>

  Prendre un mot de la langue française et le changer d’alphabet.
  Comme ce que fait tecendil_ mais en français :)

  .. class::
   :center:

   :tengwar:`riU qr&iR 13V1uR 5# ajaG5#6 jL jl5% 3`B 23ViÙ`

  .. _tecendil: https://tecendil.com/

.. raw:: html

  <style>

  @media screen and (min-width:800px) {
    fieldset div {
      display: flex;
    }
  }
  fieldset label {
    flex: 1;
    max-width: 6em;
  }
  fieldset input, fieldset select{
    flex: 1;
    width: 100%;
    box-sizing: border-box;
    font-size: 24px;
  }

  @font-face {
    font-family: "Tengwar Annatar" ;
    src: url("/pages/tengwar/tngan.ttf") format("truetype");
  }
  @font-face {
    font-family: "Tengwar Telcontar" ;
    src: url("/pages/tengwar/tengtelc.ttf") format("truetype");
  }

  .annatar {
    font-family: "Tengwar Annatar" ;
  }
  .telcontar {
    font-family: "Tengwar Telcontar"
  }


  </style>
    <noscript>Sorry, you need to enable JavaScript to see this page.</noscript>
      <script id="lib" type="text/javascript" defer="defer" src="{static}tengwar.js"></script>
    <script>
      var script = document.getElementById('lib');
      script.addEventListener('load', function() {
        lib.run("source", "phono", "tengwar", "font");
      });
    </script>

.. role:: annatar

.. raw:: html

 <fieldset>
  <div>
    <label for="source">Entrée :</label>
    <input type="text" id="source" name="source" class="telcontar">
  </div>
  <div>
  <label for="output">Phonétique :</label>
  <input type="text" id="phono" name="output">
  </div>
  <div>
  <label for="font">Rendu :</label>
  <select id="font">
    <option value="annatar">Tengwar Annatar</option>
    <option selected value="telcontar">Tengwar Telcontar</option>
  </select>
  </div>
  <div>
  <label for="output">Tengwar :</label>
  <input type="text" id="tengwar" class="telcontar" name="output">
  </div>
 </fieldset>


Saisie
======

La saisie accepte toute les lettres en minuscules, et les accents sur la lettre E : *é*, *è*, *ê*

Séparateur
----------

Le séparateur *|* permet de marquer une rupture entre les syllabes et empêcher
que le traitement ne les associe entre-elles :

======= =======================
Code    Exemple
======= =======================
\|      co|incider, ag|nostique
======= =======================

Transcription
=============

La transcription est réalisée en suivant le mode français du Tengwar qui est
dérit sur cette page : http://www.simonrousseau.free.fr/tolkien/

Il s’agit d’un mode dans lequel la voyelle est écrite au dessus de la consonne
qu’elle précède : :annatar:`7Y` va se lire *or*.

Sons
====

L’application convertit les mots saisis dans l’alphabet inventé par Tolkien… ou
du moins essaie : étant donné que la prononciation des mots n’est pas fixe (et
une même peut être prononcée de plusieurs manières *être fier*, *se fier*), une
transcription phonétique est proposée, pour indiquer comment l’application a
compris le mot. En trichant avec l’orthographe, il est possible d’obtenir
l’écriture comme on le souhaite.

Voyelles
--------

.. table::
    :widths: 5 20 10 10

    ======= =========== =========================== ==============
    Code    Exemple     Son                         Représentation
    ======= =========== =========================== ==============
    a       bat, plat   a                           :annatar:`\`C`
    i       lit, émis   i                           :annatar:`\`B`
    y       lu          u                           :annatar:`\`Û`
    u       roue        ou                          :annatar:`\`M`
    o       peau, fort  o                           :annatar:`\`N`
    e       été         e fermé                     :annatar:`\`V`
    ɛ       pair        e ouvert                    :annatar:`\`V`
    ə       abordera    schwa                       :annatar:`\`Ë`
    ø       deux        e fermé                     :annatar:`\`ß`
    œ       neuf        e ouvert                    :annatar:`\`ß`
    ɛ̃       cinq        in                          :annatar:`5%`  [#nasal]_
    œ̃       parfum      un                          :annatar:`5Ø`  [#nasal]_
    ɑ̃       ange        an                          :annatar:`5#`  [#nasal]_
    ɔ̃       savon       on                          :annatar:`5^`  [#nasal]_
    j [#d]_ famille     y (semi-voyelle)            :annatar:`l` [#next]_
    ɥ [#d]_ huit        ui (semi-voyelle)           :annatar:`]` [#next]_
    w [#d]_ nouer       w (semi-voyelle)            :annatar:`.` [#next]_
    ======= =========== =========================== ==============

.. [#nasal] Les voyelles nasales sont représentées à l’aide de la voyelle et
            d’une consonne nasale

.. [#d]     Les diphtongues sont représentées groupées avec la voyelle qui la
            suit `[wE]b`

.. [#next]  Contrairement au mode décrit par Simon Rousseau, la voyelle est
            portée sur le tengwa qui suit la semi-voyelle.

Consonnes
---------

.. table::
    :widths: 5 20 10 10

    ======= =========== =============== ==============
    Code    Exemple     Son             Représentation
    ======= =========== =============== ==============
    p       soupe       p (occlusive)   :annatar:`q` (:annatar:`y`) [#m]_
    b       bon, robe   b (occlusive)   :annatar:`w` (:annatar:`y`) [#m]_
    t       terre, vite t (occlusive)   :annatar:`1` (:annatar:`6`) [#m]_
    d       dans, aide  d (occlusive)   :annatar:`2` (:annatar:`6`) [#m]_
    k       carré       k (occlusive)   :annatar:`a`
    g       gare        g (occlusive)   :annatar:`s`
    f       feu, éfrit  f (fricative)   :annatar:`e`
    v       vous        v (fricative)   :annatar:`r`
    s       soucis      s (fricative)   :annatar:`3` (:annatar:`i`) [#m]_
    z       maison      z (fricative)   :annatar:`4`
    ʃ       chat, pèche ch (fricative)  :annatar:`d`
    ʒ       mijoter     ge (fricative)  :annatar:`f`
    n       nous        n (nasale)      :annatar:`5`
    m       main        m (nasale)      :annatar:`t`
    ɲ       agneau      gn (nasale)     :annatar:`b`
    l       sol, lent   l (liquide)     :annatar:`j`
    ʁ       rue, venir  r (liquide)     :annatar:`7` (:annatar:`u`) [#m]_
    ======= =========== =============== ==============

.. [#m]     La lettre entre parenthèse correspond à la consonne muette pouvant
            se trouver en fin de mot. Elle ne s’entend que dans le cas d’une
            liaison : `d@(s)`
