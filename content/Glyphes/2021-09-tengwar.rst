.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

=================
Écrire en tengwar
=================

:date: 2021-09-17
:tags: javascript
:summary: |summary|

.. default-role:: literal

.. |summary| replace::

  Une nouvelle application est apparue sur le blog, qui permet de transcrire
  des mots dans l’alphabet Tengwar. Je décris les coulisses et le travail qui
  se trouve derrière.

.. raw:: html

  <style>
  @font-face {
    font-family: "Tengwar Annatar" ;
    src: url("/pages/tengwar/tngan.ttf") format("truetype");
  }

  .tengwar {
    font-family: "Tengwar Annatar" ;
  }
  </style>

.. role:: tengwar

Du roman au réel
================

C’est en suivant le chemin de la calligraphie que je suis tombé sur l’écriture
en *tengwar* inventée par Tolkien pour les langues dans le monde du seigneur
des anneaux. Ce qui m’a plu de suite dans son alphabet était la correspondance
entre la représentation des lettres et les sons auxquels ils sont associés
(consonnes voisées, etc)

Dès sa conception, Tolkien avait envisagé que le même alphabet puisse être
utilisé dans les différentes langues de son œuvre, et avait prévu que des
variations puissent être apportées dans la prononciation en fonction de la
langue en question. Quand on y réfléchit, c’est la même chose avec notre
alphabet latin, et nous sommes tout à fait capables de changer notre lecture dès
que nous voyons un mot écrit en anglais pour le lire correctement.

Ces *modes* des tengwar ouvrent la portes à l’écriture dans le monde réel,
puisqu’il est possible de concevoir un mode de lecture qui serait adapté aux
langues vivantes, et plusieurs personnes s’y sont collées, chacune avec leur
propre interprétation.

Bien sur, l’informatique n’est pas en reste, et nous avons également de
nombreuses polices d’écriture qui permettent également d’afficher les tengwar à
l’écran. En combinant les deux j’ai donc essayé de créer une |application|__ qui,
à partir d’un mot de la langue française, transcrit celui-ci dans l’alphabet
tengwar. Cette application doit donc réaliser deux opérations :

.. |application| replace:: application (:tengwar:`aF77TÉ jiR 1g%s.7E`)

.. __: {filename}/pages/tengwar/tengwar.rst

1. À partir de l’écriture d’un mot, déterminer sa prononciation
2. À partir de sa prononciation, afficher les caractères dans la (une) police
   en tengwar selon le mode français.

Eh bien la partie 2 était en fait la plus simple :)

La complexité du réel
=====================

C’est bien connu, l’informatique ne se contente pas d’à peu près. Il a donc
fallu me replonger dans quelque chose qui est tellement évident que nous le
faisons sans y penser : comment faisons-nous pour lire les mots comme ils sont,
et surtout *comment faisons-nous pour lire un mot que nous ne connaissons pas
?* Bienvenue dans le monde du français, où la prononciation du `S` se
transforme selon les lettres qui l’entourent, où le `N` vient fusionner avec la
voyelle qui le précède, et les `U` s’effacent derrière les `G`, `Q`…

Il est illusoire de vouloir obtenir quelque chose de parfait. En fait, la
prononciation correcte d’un mot nécessite de connaitre sa nature ; par exemple
*aiment* et *vraiment* ne different que deux lettres mais ont une prononciation
complètement différente (et ne parlons pas de *se fier* ou *être fier*). Hors
de tout contexte, il est donc impossible de viser juste (et cela pose question
sur le processus cognitif que l’on met en place quand nous lisons un texte…)

Heureusement, on peut quand même obtenir des résultats satisfaisants. Sans
entrer dans le détail, j’ai du en fait mettre trois opérations pour réaliser
mon application :

1. Une transformation des lettres vers les sons,
2. Une transformation des sons en syllabes,
3. Des traitements d’homogénisation sur les syllabes.

Pour la deuxième étape, la page de Wikipédia sur la `structure syllabique du
français`__ a été la clef qui m’a permis d’organiser mon programme.

.. __: https://fr.m.wiktionary.org/wiki/Annexe:Prononciation/fran%C3%A7ais#Structure_syllabique

Retour aux lettres
==================

Ceci fait, il ne reste plus qu’à définir une dernière transformation pour
représenter chaque son dans un alphabet dédié. Le plus difficile étant fait, il
est même possible d’en définir deux ! Un qui servira à contrôler que
l’application a correctement compris notre mot, et un second qui sera
l’affichage en tengwar proprement dit. On pourrait également envisager d’autres
alphabets, comme l’aurebesh :)

.. Un problème toutefois : étant donné qu’il ne s’agit pas d’un alphabet
   officiel, celui-ci n’a jamais été normalisé, et par conséquent les
   différentes polices ont construit leurs caractères par-dessus les caractères
   existants.  Contrairement à l’alphabet latin dans lequel la lettre `A` sera
   toujours associée à la même représentation, nous avons ici une police qui
   vient s’afficher à la place des caractères standard habituels. Cela n’aide
   pas à l’écriture d’un texte…

