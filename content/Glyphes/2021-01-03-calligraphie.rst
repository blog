.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

================================
De la calligraphie en javascript
================================

:date: 2021-01-03
:tags: javascript
:summary: |summary|

.. default-role:: literal

.. |summary| replace::

 Un petit bilan après avoir réalisé une application de calligraphie sur le
 blog :)

À la suite de mon article précédent, j'ai essayé d'aller un peu plus loin dans
ce qu'il était possible de faire en javascript. La page pour construire les
graphes montrait déjà qu'il était possible de charger du javascript dans la
page, et modifier directement le contenu du blog à partir du code exécuté.

En suivant toujours la même démarche (code en OCaml, compilé en javascript),
j'ai mis en place une *ardoise* dans laquelle je souhaitais retrouver sur PC le
rendu que l'on peut avoir avec une plume dans la main : `l'ardoise calligraphique`_.

.. _l'ardoise calligraphique: {filename}/pages/script.it/script.rst#ardoise-calligraphique

Ce que j'ai appris
==================

Les courbes de beziers ! Je ne pensais pas qu'il y aurait autant à faire avec !
Pour ceux que cela intéresse, la page `A Primer on Bézier Curves`_ est une
référence avec tout ce qu'il faut pour mettre en place ses propres librairies.

.. _A Primer on Bézier Curves: https://pomax.github.io/bezierinfo/

Afin d'avoir un tracé souple de la courbe, j'ai suivi un document de l'UCLA_
qui explique comment construire les courbes de beziers en suivant une série de
points, ce qui m'a fait faire un détour par les produits matriciels pour
résoudre ce genre d'équations :

.. _ucla: https://www.math.ucla.edu/~baker/149.1.02w/handouts/dd_splines.pdf

.. math::

 \begin{bmatrix}4 && 1 && 0 && 0 \\\\ 1 && 4 && 1 && 0 \\\\0 && 1 && 4  && 1 \\\\0 && 0 && 1 && 4  \end{bmatrix}
 \begin{bmatrix} B_1 \\\\ B_2 \\\\ B_3 \\\\ B_4 \end{bmatrix}
 =
 \begin{bmatrix} (6 S_1 - S_0)  \\\\ 6S_2 \\\\ 6 S_3 \\\\ (6S_4 - S_5) \end{bmatrix}

A priori, il n'existe pas de librairie pour faire de la réduction de matrice en
javascript, mais l'avantage d'utiliser OCaml est d'avoir sous la main `quelques
librairies`_ permettant de faire le boulot directement.

.. _quelques librairies: https://github.com/kandluis/ocaml-matrix

Ensuite, travailler le DOM. Vu que le site est statique, j'ai choisi de
modifier la page directement à partir du code javascript. En ajoutant un
élément, en retirant ceux dont je n'avais nécessité, cela permet de changer
l'apparence directement pour se construire son application. Cela m'a permis de
me plonger dans la librairie `brr`_ qui permet d'interagir avec la page via
des évènements fonctionnels.

.. _brr: https://erratique.ch/software/brr

Enfin, les webWorkers_, qui sont des threads pouvant être lancés dans le code
javascript. Dans l'application, je les utilise pour *lisser* la courbe une fois
que celle-ci a été construite une première fois.

.. _webworkers: https://developer.mozilla.org/fr/docs/Web/API/Web_Workers_API/Utilisation_des_web_workers

Ce qu'il faudrait continuer
===========================

Il me manque tout un cadre mathématique pour aller plus loin, j'aurai par
exemple besoin de déterminer l'extrémité_ exacte de la courbe afin de traiter
ces cas de manière précise, et j'ai besoin de comprendre le code avant de m'y
mettre…

.. _extrémité: https://pomax.github.io/bezierinfo/#extremities

Par conséquent, l'export SVG est un peu biaisé, on retrouve bien ce qui est
affiché sur l'écran, mais cela donne quelque chose qui n'est pas vraiment
éditable comme je l'aurai souhaité.

Il faudrait aussi aller plus loin dans les mouvements de plumes, permettre des
rotations locales, juste sur un segment et non pas sur la totalité du tracé,
enfin des petits ajustements pour qu'il y ait vraiment une plus-value.

D'autres pistes
===============

Cela m'a aussi donné d'autres pistes pour continuer avec javascript et le blog.
En utilisant l'API `Web Storage`_, on peut également stocker des données comme
si l'on travaillait sur un fichier, les données étant stockées dans l'espace du
navigateur. En combinant cela avec le côté statique du site, je pense à une
page de prise de note, qui dont le code serait uniquement exécuté dans le
navigateur, sans avoir besoin de serveur comme support.

.. _web storage: https://developer.mozilla.org/fr/docs/Web/API/Web_Storage_API

Le blog offrant un espace dans lequel je peux mettre en ligne mes applications,
cela me donne envie de continuer et m'en servir comme petit terrain
d'exipérimentation…
