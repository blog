#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import colorsys

def define(saturation, value):

    def showColor(hue):
        return colorsys.hsv_to_rgb(hue, saturation, value)

    return showColor

def showColor(theme, hue):

    r, g, b = theme(hue / 360.)
    return "#%02x%02x%02x" % (int(r*256), int(g*256), int(b*256))

if __name__ == "__main__":

    if len(sys.argv) < 5:
        print "Usage :"
        print "color.py SHIFT LIGHT_SATURATION DARK_SATURATION LIGHT_VALUE DARK_VALUE"
        sys.exit(1)

    shift = int(sys.argv[1])
    light_s = int(sys.argv[2])/100.
    dark_s =  int(sys.argv[3])/100.
    light_v = int(sys.argv[4])/100.
    dark_v =  int(sys.argv[5])/100.

    colors = {

        1 : 0,    # red
        3 : 60,   # yellow
        2 : 120,   # green
        6 : 180,  # cyan
        4 : 240,  # blue
        5 : 300,  # magenta

    }

    dark_theme = define(dark_s, dark_v)
    light_theme = define(light_s, light_v)
    for (name, value) in colors.iteritems():

        dark  = showColor(dark_theme, value - shift)
        light = showColor(light_theme, value - shift)

        print "urxvt.color%d : %s" % (name, dark)
        print "urxvt.color%d : %s" % (name+8, light)

        print "XTerm*color%d : %s" % (name, dark)
        print "XTerm*color%d : %s" % (name+8, light)


