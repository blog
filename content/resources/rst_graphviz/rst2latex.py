#!/usr/bin/python

# $Id: rst2latex.py 5905 2009-04-16 12:04:49Z milde $
# Author: David Goodger <goodger@python.org>
# Copyright: This module has been placed in the public domain.

"""
A minimal front end to the Docutils Publisher, producing LaTeX.
"""

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

import subprocess
import os.path

from docutils.core import publish_cmdline
from docutils.parsers.rst import directives
from docutils.parsers.rst.directives.images import Figure, Image

description = ('Generates LaTeX documents from standalone reStructuredText '
               'sources. '
               'Reads from <source> (default is stdin) and writes to '
               '<destination> (default is stdout).  See '
               '<http://docutils.sourceforge.net/docs/user/latex.html> for '
               'the full reference.')

DOT2TEX = "dot2tex"

def processContent(node):

    if node.content.count("..") == 0:
        sep = -1 
        text = '\n'.join(node.content)
    else:
        sep = node.content.index("..")
        text = '\n'.join(node.content[:sep])
    extension = 'pdf'
    pdfFile = "tmp/%s.pdf" % (hash(text))


    if not os.path.exists(pdfFile):
        conversion = subprocess.Popen(['dot',
            '-T', extension,
            "-Gcharset=utf8",
            ],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            )

        ps2pdf = subprocess.Popen(['ps2pdf',
            "-",
            pdfFile
            ],
            stdin = conversion.stdout
            )
        conversion.stdin.write("%s G {\n" %
                node.arguments[0].encode("utf-8"))
        conversion.stdin.write(text.encode("utf-8"))
        conversion.stdin.write("\n}")
        conversion.stdin.close()
        conversion.stdout.close()

        ps2pdf.wait()
        conversion.wait()
        if conversion.returncode != 0:
            raise node.error(
                'Error in "%s" directive:\n' % node.name)

    return (sep, pdfFile)

class GraphvizImage(Image):
    """ Generate a graphviz image
    """
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    has_content = True

    option_spec = Image.option_spec.copy()

    def run(self):


        sep, arguments = processContent(self)
        self.arguments = [arguments]
        self.options['scale'] = 66
        if sep == -1:
            self.content = None
        else:
            self.content = self.content[sep+1:]

        return Image.run(self)

class Graphviz(Figure):
    """ Generate a graphviz image
    """
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    has_content = True

    option_spec = Figure.option_spec.copy()

    def run(self):

        sep, arguments = processContent(self)
        self.arguments = [arguments]
        self.options['scale'] = 66
        if sep == -1:
            self.content = None
        else:
            self.content = self.content[sep+1:]

        return Figure.run(self)


class Dot2Tex(Figure):
    """ Generate a graphviz image using dot2tex
    """
    required_arguments = 1
    optional_arguments = 0
    final_argument_whitespace = True

    has_content = True

    option_spec = Figure.option_spec.copy()

    def run(self):

        if self.content.count("..") == 0:
            sep = -1 
            text = '\n'.join(self.content)
        else:
            sep = self.content.index("..")
            text = '\n'.join(self.content[:sep])
        pdfFile = "tmp/%s.pdf" % (hash(text))
        texFile = "tmp/%s.tex" % (hash(text))


        if not os.path.exists(pdfFile):
            conversion = subprocess.Popen([DOT2TEX,
                '-c',
                '-o', texFile,
                ],
                stdin=subprocess.PIPE
                )
            conversion.communicate("%s G { \n %s \n}" 
                    % (self.arguments[0], text))
            conversion.wait()

            ps2pdf = subprocess.Popen(['pdflatex',
                '-output-directory', 'tmp',
                texFile,
                ],
                )
            ps2pdf.wait()
            if conversion.returncode != 0:
                raise self.error(
                    'Error in "%s" directive:\n' % self.name)

        self.arguments = [pdfFile]
        self.options['scale'] = 66
        if sep == -1:
            self.content = None
        else:
            self.content = self.content[sep+1:]

        return Figure.run(self)

#try:
#    # Check if dot2tex exists and register the conversion with dot2tex and
#    # pdftex
#    subprocess.check_call([DOT2TEX, "-V"])
#    directives.register_directive('graphviz', Dot2Tex)
#    directives.register_directive('graphimg', GraphvizImage)
#except:
#    # Otherwise use the old graphviz program
directives.register_directive('graphviz', Graphviz)
directives.register_directive('graphimg', GraphvizImage)

publish_cmdline(writer_name='latex', description=description)
