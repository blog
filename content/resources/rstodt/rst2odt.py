#!/usr/bin/python

# $Id: rst2odt.py 5839 2009-01-07 19:09:28Z dkuhlman $
# Author: Dave Kuhlman <dkuhlman@rexx.com>
# Copyright: This module has been placed in the public domain.

"""
A front end to the Docutils Publisher, producing OpenOffice documents.
"""

try:
    import locale
    locale.setlocale(locale.LC_ALL, '')
except:
    pass

from docutils.core import publish_cmdline_to_binary, default_description
from docutils.writers.odf_odt import Writer, Reader

from docutils.parsers.rst.roles import set_classes

from docutils import nodes
from docutils.parsers.rst import directives, Directive


description = ('Generates OpenDocument/OpenOffice/ODF documents from '
               'standalone reStructuredText sources.  ' + default_description)

class Codeblock(Directive):
    """ Source code syntax highlighting.
    """
    required_arguments = 1
    optional_arguments = 1
    final_argument_whitespace = True

    has_content = True
    linenos=1

    directives = {":include:": lambda self, *args, **kwargs : self.include(*args, **kwargs)}

    def include(self, param):
        """ Include a file and return it as a content.
        """
        try:
            self.content = unicode(open(param, 'r').read(), "utf-8").split("\n")
        except IOError:
            self.content = ("FileNotFound",)

    def run(self):
        for argument in self.arguments:
            directives = argument.split(" ")
            processor = self.directives.get(directives[0], None)
            if processor is None:
                continue
            processor(self, directives[1])

        set_classes(self.options)
        self.assert_has_content()
        text = '\n'.join(self.content)
        text_nodes, messages = self.state.inline_text(text, self.lineno)
        node = nodes.literal_block(text, '', *text_nodes, **self.options)
        node.line = self.content_offset + 1
        self.add_name(node)
        node.attributes['language'] = self.arguments[0]

        return [node] + messages


directives.register_directive('code-block', Codeblock)


writer = Writer()
reader = Reader()
output = publish_cmdline_to_binary(reader=reader, writer=writer,
    description=description)


