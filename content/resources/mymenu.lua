-- Menu d'application

local awbeautiful = require("beautiful")
local awful = require("awful")
local capi     = {
    screen     = screen,
}

local function getStickyIcon(client)
    if client.sticky then
        return awbeautiful.get().titlebar_sticky_button_focus_active
    end
    return awbeautiful.get().titlebar_sticky_button_focus_inactive
end

local function getOnTopIcon(client)
    if client.ontop then
        return awbeautiful.get().titlebar_ontop_button_focus_active
    end
    return awbeautiful.get().titlebar_ontop_button_focus_inactive
end

function newAppMenu(client)
    local data = {}
    local myAppMenu = {}

    local args = {}
    args.keygrabber = true

    -- Insert the ontop entry
    -- When onTop is selelected, set in floating mode too
   table.insert(myAppMenu, {"On Top", function()
        -- We do not use floating.togle() but syncronize the floating on top value
        awful.client.floating.set( client, not client.ontop ) 
        client.ontop = not client.ontop
    end , getOnTopIcon(client)}  )

    -- Insert the Sticky entry
   table.insert(myAppMenu, {"Sticky", function() client.sticky = not client.sticky end , getStickyIcon(client)}  )

    -- Insert the Hide entry
    if not client.minimized then
        table.insert(myAppMenu, {"Minimize", function() client.minimized = true end } )
    end
    table.insert(myAppMenu, {"Close", function() client:kill() end, awbeautiful.get().titlebar_close_button_focus } )

    -- Add the move to tag entry
    local moveToTag = {}
    local tags = capi.screen[1]:tags()
    for i = 1, #tags do
        -- We do not show the curent client tags
        local isInTag = false
        local clientTag = tags[i]:clients()
        for j = 1, #clientTag do
            isInTag = isInTag or clientTag[j] == client
        end
        if not isInTag then
            table.insert(moveToTag, {tags[i].name, function() awful.client.movetotag(tags[i], client) end } )
        end
    end
    table.insert(myAppMenu, {"Move To", moveToTag } )

    data.menu = awful.menu.new( { items = myAppMenu} )
    data.menu:show(args)

    return data
end
