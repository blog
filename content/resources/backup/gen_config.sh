#!/bin/bash

. $(dirname $1)/./$(basename $1)
OUTFILE=$2
BACKUP_NAME=$3

if test "x${PCA_OS_REGION_NAME}" = x; then
  PCA_OS_REGION_NAME=${OS_REGION_NAME}
fi

if test "x${HOT_OS_REGION_NAME}" = x; then
  HOT_OS_REGION_NAME=${OS_REGION_NAME}
fi

envsubst > "${OUTFILE}" << EOF
[
    {
        "description": "Cold storage",
        "url": "pca://${BACKUP_NAME}",
        "env": [
            {
                "name": "PCA_AUTHURL",
                "value": "${OS_AUTH_URL}"
            },
            {
                "name": "PCA_AUTHVERSION",
                "value": "${OS_IDENTITY_API_VERSION}"
            },
            {
                "name": "PCA_PROJECT_DOMAIN_NAME",
                "value": "Default"
            },
            {
                "name": "PCA_TENANTID",
                "value": "${OS_TENANT_ID}"
            },
            {
                "name": "PCA_USERNAME",
                "value": "${OS_USERNAME}"
            },
            {
                "name": "PCA_PASSWORD",
                "value": "${OS_PASSWORD}"
            },
            {
                "name": "PCA_REGIONNAME",
                "value": "${PCA_OS_REGION_NAME}"
            }
        ],
        "prefixes": ["cold_"]
    },
    {
        "description": "Hot storage",
        "url": "swift://${BACKUP_NAME}_indexes",
        "env": [
            {
                "name": "SWIFT_AUTHURL",
                "value": "${OS_AUTH_URL}"
            },
            {
                "name": "SWIFT_AUTHVERSION",
                "value": "${OS_IDENTITY_API_VERSION}"
            },
            {
                "name": "SWIFT_PROJECT_DOMAIN_NAME",
                "value": "${OS_PROJECT_DOMAIN_NAME}"
            },
            {
                "name": "SWIFT_TENANTID",
                "value": "${OS_TENANT_ID}"
            },
            {
                "name": "SWIFT_USERNAME",
                "value": "${OS_USERNAME}"
            },
            {
                "name": "SWIFT_PASSWORD",
                "value": "${OS_PASSWORD}"
            },
            {
                "name": "SWIFT_REGIONNAME",
                "value": "${HOT_OS_REGION_NAME}"
            }
        ],
        "prefixes": ["hot_"]
    }
]
EOF
