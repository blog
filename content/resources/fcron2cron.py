#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import subprocess
import os

FCRON_LIST="/var/spool/fcron"

def transform_variable(variables):
    """ Transform a list of variables like
    bootrun(true),nice(15),serial(true)
    """

    variable_operators = {
            'mail': lambda x: x == "false" and 'MAILTO=""' or "",
            'mailto': lambda x : 'MAILTO="%s"' %x,

        }

    transforms = ""

    for variable in variables.split(","):
        attribute, value = variable.split('(')
        transforms += variable_operators.get(attribute, lambda x:"")(value[:-1])
    return transforms

def transform_directive(line):
    operators = {
            'hourly':       ('@hourly', 2),
            'midhourly':    ('@hourly', 2),
            'daily':        ('@daily',  3),
            'middaily':     ('@daily',  3),
            'nightly':      ('@daily',  3),
            'weekly':       ('@weekly', 3),
            'midweekly':    ('@weekly', 3),
            'montly':       ('@montly', 4),
            'midmonthly':   ('@montly', 4),
        }
    operator = line.split(" ")[0].split(',')[0]
    substitute, to_trim = operators.get(operator, ("", 0))
    return "%s\t%s" %(substitute, line.split(None, to_trim)[to_trim])

def transform(line):
    if line == '':
        return ''
    transformation = {
            '!': lambda x:transform_variable(x[1:]),
            '%': lambda x:transform_directive(x[1:]),
            '&': lambda x: x.split(None, 1)[1],
            }
    return transformation.get(line[0], lambda x:x)(line)

def main(parameters):
    configs = os.listdir(FCRON_LIST)
    for config in configs:
        process = subprocess.Popen(["fcrontab", "-l", config], universal_newlines=True, stdout=subprocess.PIPE)
        f = open('%s.crontab' % config, 'w')
        for line in process.stdout:
            transformation = transform(line.strip().expandtabs(1))
            if transformation is not None:
                f.write("%s\n" % transformation)
        f.close()


if __name__ == "__main__":
    main(sys.argv)

