:title: Nonfree firmwares for Android devices
:date: 2017-01-07 22:27
:modified: 2017-03-04 19:30
:tags: scripting, nonfree software
:category: General
:author: Wolfgang Wiedmeyer
:status: published
:summary: The following instructions make it possible to install nonfree firmware on Android devices.

The following instructions make it possible to install nonfree firmware on Android devices for the purpose of enabling certain functionalities. Please see `Paul's blog post <http://code.paulk.fr/article16/missing-proprietary-firmwares-in-android-systems>`_ for the reasons why these instructions shouldn't be published on official documentation pages of free systems, but why it still makes sense to publish them elsewhere.

Paul's script is compatible with CyanogenMod versions 10.1.3 and 9.1.0. It extracts the firmwares from a CyanogenMod installation zip and either installs them with ADB or creates a new installation zip with them. My implementation is intended to be compatible with LineageOS/CyanogenMod 13.0. It's possible to only select a certain functionality for which the firmwares should be installed. The firmwares are downloaded from a `repository <https://github.com/TheMuppets>`_, that hosts the firmware files for CyanogenMod builds, and they are installed with ADB.

Downloading the firmware files seemed to be the most straightforward solution since it's impractical to extract them from a CyanogenMod 13.0 installation zip because recent Android versions use complicated compression techniques that require tools which are usually not readily available as packages for GNU/Linux systems. Creating installation zips is also not easy anymore because the zip needs to be signed with a key that is trusted by the recovery. If your recovery still accepts any installation zip, then you are using an insecure recovery.

Make sure that you have ADB and root access with ADB enabled.
These steps are required:

1. Clone the `firmwares_nonfree repository <https://code.fossencdi.org/firmwares_nonfree.git/>`_:

   .. code-block:: console

      $ git clone https://code.fossencdi.org/firmwares_nonfree.git

2. Connect the device to your PC and check if the device is detected:

   .. code-block:: console

      $ adb devices

   If the list of attached devices is empty, you will have to check your ADB setup first and figure out why the device doesn't get recognized.

3. Run the script in the git repository with two arguments, the name of your device and the functionality you want to enable, e.g.:
      
   .. code-block:: console

      $ ./firmwares.sh i9300 wifi

   This command will only install the firmware files that are needed to enable wifi. The firmware will be downloaded, pushed to the device and the device is rebooted. Use ``list`` as second argument to get a list of functionalities that can be enabled for the specific device. You can also specify ``all`` to install all firmware files which will enable all listed functionalities.

4. If you want to remove a certain firmware again, you can do so by adding ``remove`` as third argument, e.g.:

   .. code-block:: console

      $ ./firmwares.sh i9300 front-camera remove

   This command deletes the firmware files for the front camera from the device and reboots it.
