:title: Setting up a Debian Stretch build environment for Replicant 6.0 using debootstrap
:date: 2016-12-28 23:10
:modified: 2016-05-07 18:10
:tags: debootstrap, toolchain, free software
:category: General
:author: Wolfgang Wiedmeyer
:status: published
:summary: Recently, I managed to to make `Replicant 6.0 <https://blog.replicant.us/2016/08/replicant-6-early-work-upstream-work-and-f-droid-issue/>`_ buildable on Debian Stretch. I'm still using Debian Jessie on my machines, so I set up a chroot with Stretch. I'll document all the necessary steps in the following to establish a Stretch build environment and build Replicant 6.0 inside it.

Recently, I managed to make `Replicant 6.0 <https://blog.replicant.us/2016/08/replicant-6-early-work-upstream-work-and-f-droid-issue/>`_ buildable on Debian Stretch. This makes it possible to use a lot of new Android-related packages in Debian. I'm still using Debian Jessie on my machines, so I set up a chroot with Stretch. I'll document all the necessary steps in the following to establish a Stretch build environment and build Replicant 6.0 inside it. This should work across many distros, although I only tested it on Debian Jessie. The only tool you'll need is a `debootstrap <https://wiki.debian.org/Debootstrap>`_ that contains the necessary scripts to set up a Debian Stretch chroot.

Installing Debian Stretch in a chroot with debootstrap
######################################################

First install debootstrap. I recommend getting the latest version that is available for you, e.g. from `Jessie Backports <https://packages.debian.org/jessie-backports/debootstrap>`__. Then create a directory where you want the chroot to be.

| `If you are using a kernel with grsecurity`:
| `Grsecurity <https://grsecurity.net/>`_ does quite some hardening of chroots. Unfortunately, we need to disable a few of the configuration options to make debootstrap work. You will need to deactivate the following three in /etc/sysctl.d/grsec.conf:
| ``kernel.grsecurity.chroot_deny_chmod``
| ``kernel.grsecurity.chroot_caps``
| ``kernel.grsecurity.chroot_deny_mount``
| Depending on your configuration, a reboot might be necessary.
|

Then you can run debootstrap to set up a minimal Stretch install. The command needs to be run as root, while ``$YOUR_CHROOT_DIR`` is the absolute path to the directory you created for the chroot:

.. code-block:: console

   # debootstrap stretch $YOUR_CHROOT_DIR

The proc filesystem needs to be mounted inside the chroot:

.. code-block:: console

   # mount proc $YOUR_CHROOT_DIR/proc/  -t proc

Every time you enter the chroot to build Replicant 6.0, you will need to mount the proc filesystem. So I recommend adding a permanent entry in your ``/etc/fstab``::

  proc $YOUR_CHROOT_DIR/proc proc defaults 0 0

If you don't mount the proc filesystem, you will get very weird build errors and it might take some time until you figure out that it's because proc is not mounted. Trust me, I was already there.

For initial setup, devpts also needs to be mounted:

.. code-block:: console

   # mount --bind /dev/pts $YOUR_CHROOT_DIR/dev/pts

Then you can finally enter the chroot:

.. code-block:: console

   # chroot $YOUR_CHROOT_DIR

Tell apt where it should get the source packages from:

.. code-block:: console

   # echo "deb-src http://deb.debian.org/debian stretch main" >> /etc/apt/sources.list

Replacing ``deb.debian.org`` in your ``sources.list`` with a local mirror likely leads to a lot faster downloads and puts less load on the Debian servers. I'm in Germany so I have ``ftp.de.debian.org`` in my ``sources.list``.

Then add the i386 architecture and update:

.. code-block:: console

   # dpkg --add-architecture i386
   # apt-get update

Before installing all the necessary build dependencies, locales needs to be configured correctly:

.. code-block:: console

   # apt-get install locales
   # dpkg-reconfigure locales

Then install `all dependencies <https://redmine.replicant.us/projects/replicant/wiki/BuildDependenciesInstallation#Debian-based-systems>`_.

Add a user for building Replicant 6.0:

.. code-block:: console

   # adduser $YOUR_USER

|
| `If you are using a kernel with grsecurity`:
| In case Trusted Path Execution (TPE) is enabled, you will have to recreate the group inside the chroot and add your user to it:
| groupadd -r -g 64040 grsec-tpe
| usermod -aG grsec-tpe $YOUR_USER
|

Switch to your user:

.. code-block:: console

   # su $YOUR_USER

From now on, you can follow the `respective build page for your device <https://redmine.replicant.us/projects/replicant/wiki#Replicant-build>`_ (given that Replicant 6.0 is supported on your device).

Some ``repo`` commands fail if the shared memory is not mounted inside the chroot. So if you use ``repo`` from inside the chroot, mount ``dev/shm``:

.. code-block:: console

   # mount --bind /dev/shm $YOUR_CHROOT_DIR/dev/shm
