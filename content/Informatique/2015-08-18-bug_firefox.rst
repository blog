.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

====================================
Ouvrir un rapport de bug sur firefox
====================================

:date: 2015-08-18
:tags: Humeur, Libre
:summary: |summary|

.. figure:: {static}/images/labyrinth.jpg
    :figwidth: 140
    :figclass: floatright
    :alt: Labyrinth !

    Image : `yourdoku.com`_ (creativecommons_)

.. _yourdoku.com : https://www.flickr.com/photos/yourdoku/5451101198/
.. _creativecommons: https://creativecommons.org/licenses/by/2.0/

|summary|

.. |summary| replace::


    Suite à la mise à jour de firefox, j'ai essayé de comprendre pourquoi un
    site interne ne fonctionnait plus.
    Il s'agit d'un site interne, principalement écrit en flash, qui ne
    fonctionne plus chez tous les utilisateurs ayant mis à jour le navigateur.
    Le besoin était de trouver de l'aide pour comprendre en quoi la mise à jour
    du navigateur pouvait changer le comportement du site.

Le support
==========

Première étape, aller sur le support_. Le site est joli, on choisi le logiciel
concerné, et le composant impacté :

- Navigation basique
- Installation et mise à jour
- Synchronisez vos navigateurs
- Discussion et partage
- En faire plus avec des applications
- …
- Correction de ralentissements, plantages, messages d'erreurs et autres
  problèmes

.. _support: https://support.mozilla.org/fr/

On a l'impression d'être devant un audiotel à devoir taper 1, 2 ou 3 sur le
téléphone pour être aiguillé dans la catégorie qui nous concerne. Mais ça n'est
pas ce que je veux ! Il est où le canal IRC ? Elle est où la mailing-list ?
Tout ça semble avoir disparu du site pour laisser place à une vitrine, certe
jolie, mais qui ne m'aide pas beaucoup dans ma recherche.

Bon, ça n'est probablement pas la bonne manière de faire. Reprenons.

Mailing-list
============

Recherchons les listes de diffusion. C'est un moyen d'échanger avec les
développeurs en passant par les courriels, avec un archivage de l'ensembles des
échanges déjà réalisés, je pense que je devrais trouver mon bonheur.

Mon moteur de recherche m'amène sur `la liste des listes de diffusion`_ de
mozilla. Ça parle de beaucoup de choses : Berlin-Events, fhr-dev, inbox-triage,
Memes, party-planners mais je ne vois pas où trouver une information pour
comprendre en quoi le changement de firefox provoque un dysfonctionnement sur
mon site.

.. _la liste des listes de diffusion: https://mail.mozilla.org/listinfo


En cherchant davantage, je tombe sur `support-firefox`_. Tiens, pourquoi
n'est-elle pas citée sur la page précédente ? Qu'à cela ne tienne, un petit
message qui explique mon problème, un exemple avec les traces tcpdump qui
montrent la différence de comportement entre les deux version, et un petit
courriel envoyé à la communauté pour demander de l'aide.

.. _support-firefox: https://lists.mozilla.org/listinfo/support-firefox

Comme souvent quand on n'est pas inscrit sur la liste de diffusion, je reçoie
un courriel me disant que ma demande est en validation, je serai informé de la
décision prochainement.

Puis le silence. Un jour, deux jours, trois jours… (bon je suis mauvaise langue
aussi c'était le week-end).

Je fini par recevoir un notification de rejet, avec comme motif : la liste de
diffusion est bidirectionnelle, en miroir avec un newsgroup, Donc je risque de
ne pas être alerté des réponses postées par les utilisateurs. Merci de
s'inscrire à la liste de diffusion où de poser ma question au support.

Grrrrrrr ! Effectivement, les archives de la liste de diffusion pointent vers
les groupes google. Mais c'est fait exprès pour empêcher les gens de demander
de l'aide ou quoi ? Je suis capable d'aller consulter les archives par moi-même
pour me tenir informé des réponses !!

StackOverfow
============

Le `support pour développeurs`_ nous oriente vers StackOverfow. Allons-y, de toute
façon au point où j'en suis, autant se créer un compte. À ce stade, il faut
avouer que je suis déjà bien énervé de la manière dont les choses se déroulent.
Une simple demande d'aide est en train de se transformer en parcours du
combattant labyrinthique.

.. _support pour développeurs: https://support.mozilla.org/en-US/kb/where-go-developer-support

Je commence à recevoir quelques réponses et pistes qui ne m'aident pas beaucoup
pour comprendre mon problème. Mais cela m'a déjà permis d'échanger avec
quelqu'un qui m'a apporté son aide bénévolement. (et puis c'est quoi ce
formulaire de saisi dans stackoverflow qui valide la saisie dès qu'on appuie
sur ENTRÉE : on ne peut pas écrire de paragraphes directement sur ce site ?)

Le rapport de bogue
===================

Ne trouvant d'aide nulle part, je fini par ouvrir un rapport de bogue. J'ai
tous les éléments nécessaire pour le fournir, mes traces tcpdump feront office
de site à consulter (le site est interne à une entreprise).

Bien sûr mon rapport été déjà présent dans un autre ticket, mais encore eût-il
fallu que je le trouve ! Ce problème était même connu de firefox,
puisque présenté parmi `la liste des changements`_ de la version 40 !

.. _la liste des changements: https://developer.mozilla.org/en-US/Firefox/Releases/40/Site_Compatibility$revision/901845#sect17

Attend ? Stop ! La denière version de firefox casse la compatibilité avec
certains sites de manière officielle ? J'aurais bien aimé disposer de cette
information avant de me lancer dans cette procédure…

Bilan : que devient firefox ?
=============================

Un support quasi-impossible à joindre, qui passe par des services tels que
google ou stackoverflow ; des vitrines marketing pour aiguiller les utilisateurs
dans leur prise en main de l'application ; une communication qui se focalise sur
compatibilité avec Windows 10, mais oublie de parler de régressions dans les
sites existants ; une note sur une page obscure, que l'on ne trouve que si l'on
a déjà l'information… **mais que devient firefox ?**

Est-ce qu'il est obligatoire pour un logiciel populaire de se couper de sa base
d'utilisateurs ? Est-ce le destin vers lequel se dirige nos distributions,
LibreOffice ? Je comprend que mozilla cherche à préserver sa marque, mais j'ai
l'impression que l'application est entrée dans un autre univers. Ça n'est pas
celui que j'ai l'habitude de côtoyer.

Je veux des échanges, des mailing-lists, de l'IRC ! Je ne veux pas d'un
ensemble de logiciels marketing, j'ai opté pour le bazar, pas la cathédrale.
Sauf que le bazar est en train de se transformer en centre commercial, avec des
vigiles à l'entrée, même si tout le monde est libre d'entrer…

Est-ce que je suis le seul à ressentir cette tendance, ou bien est-ce que je me
fait trop vieux ? Quelqu'un qui ne connaît pas le logiciel libre a-t-il aussi
le sentiment de mettre les pieds dans un labyrinthe ? Pourtant ici, ça n'est
pas la technique qui me manque ; je n'ose pas imaginer le parcours du
combattant de celui voulant ouvrir un rapport de bogue sur sa distribution…

