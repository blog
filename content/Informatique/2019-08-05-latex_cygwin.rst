.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==============================================
Utiliser toutes les fonts avec Xetex et Cygwin
==============================================

:date: 2019-08-05
:tags: cygwin, latex
:logo: /images/latex/xetex.png
:summary: |summary|

.. default-role:: literal

.. figure:: {static}/images/latex/xetex.png
    :figwidth: 250
    :figclass: floatleft
    :alt: Xetex logo

|summary|

.. |summary| replace::

  Quand on utilise xetex dans l'environnement cygwin, toutes les polices
  installées ne sont pas chargées par défaut. Cela vient de la configuration de
  cygwin qui par défaut, ne scanne pas les répertoires de latex à la recherche
  des polices disponibles.

La solution est simple, il faut ajouter les lignes suivantes dans le fichier
`/etc/fonts/fonts.conf` :

.. code-block:: xml

	<dir>/usr/share/texmf-dist/fonts/opentype</dir>
	<dir>/usr/share/texmf-dist/fonts/truetype</dir>

et compléter en rechargeant les polices installées :

.. code-block:: console

        $ fc-cache -f

Fini !
