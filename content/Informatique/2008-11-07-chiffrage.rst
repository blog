.. -*- mode: rst -*-
.. -*- coding: utf-8 -*-

Mettre en place une solution de chiffrage avec authentification forte
#####################################################################

:date: 2008/11/07
:tags: Libre, ViePrivée
:summary: |summary|

.. |summary| replace::
    Voici une méthode que j'ai mise en place pour protéger l'accès à mon
    répertoire /home via Cryptsetup. Il existe de nombreux tutoriels expliquant
    comment créer une partition chiffrée, je ne vais pas détailler cela ici.
    L'intérêt du script que je propose ici est qu'il se lance au moment du
    login, et qu'il va chercher le mot de passe sur une clef USB.

|summary|

Charger le mot de passe contenu dans la clef USB
------------------------------------------------

Il suffit juste de l'insérer avant de se logguer, et de la retirer une
fois l'authentification réussie.

Voici le script en question

.. code-block:: bash

    #!/bin/sh

    #Si la clef n'est pas insérée, refuse le login
    [ -b /dev/disk/by-uuid/chemin-de-la-clef ] || exit 1

    if [ -b /dev/mapper/home ]
        then
            #Si la partition est deja montée, connecte
            exit 0
        else

            # Monte la clef
            mkdir /tmp/clef/
            mount /dev/disk/by-uuid/chemin-de-la-clef /tmp/clef/ 2>/tmp/out

            # Déchiffre la partition
            /sbin/cryptsetup luksOpen -d /tmp/clef/clef /dev/disk/by-uuid/chemin-de-la-clef home 2>>/tmp/out

            # Démonte la clef, elle peut maintenant être retirée sans pb
            umount /tmp/clef
            rmdir /tmp/clef/

            mount -o defaults -t ext2 /dev/mapper/home /home
            exit 0

    fi

Bien sûr, il faut que la clef soit contenue dans le fichier clef sur le
périphérique USB.

Mettre le script au login de l'utilisateur
------------------------------------------

Maintenant, 2e étape, il s'agit de lancer ce script au moment où
l'utilisateur vient de se logguer. Comme je passe par un login manager
sur mon portable (GDM), j'ai choisi d'utiliser ses ressources pour
lancer le script. En effet, GDM possède des scripts qui sont lancés aux
différents moments de la connexion. Ce qui nous intéresse ici se trouve
dans le fichier : `/etc/gdm/PostLogin/Default`

Il suffit d'y coller notre script (ou d'y faire appel), et notre partition
sera activée automatiquement lors de la connexion (et seulement si le mot de
passe est valide). 

On peut obtenir des paramètres de la part de GDM dans ce script, nom de
l'utilisateur qui se loggue, répertoire de login etc, cela permet de
personnaliser notre script si on le souhaite. Un code de retour en erreur
refusera le login, que le mot de passe entré soit bon ou non…
