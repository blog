.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

================================
Sauvegarder sa clef privée GPG
================================

:date: 2021-02-13
:tags: gpg
:summary: |summary|

.. default-role:: literal

.. |summary| replace::

  Comment exporter et sauvegarder sa clef privée proprement ? Le papier,
  toujours du papier…

Comme beaucoup sûrement, je chiffre mes sauvegardes avant de les envoyer dans
le cloud, je chiffre mes mots de passes (et utilise une clef sécurisée pour les
lire), et utilise pour cela l'outil gpg. Cela me permet d'être sûr que les
fichiers ne seront pas lus si l'on ne dispose pas de la clef de déchiffrement
adéquate.

Mais comment sauvegarder sa clef privée ? Est-ce que l'on peut la copier sur
une clef USB et la laisser dans un tiroir ? Comment s'assurer que la sauvegarde
sera pérenne ?

On recommande souvent l'outil paperkey pour cela, il génère un document texte
simple, pouvant être utilisé pour reconstruire la clef privée à partir de le
clef publique (en partant du principe que ladite clef publique est publique et
peut être retrouvée facilement). Le document peut ensuite être imprimé, et un
logiciel OCR permet ensuite de le régénérer.

Je propose une autre solution, basée sur le même principe, mais en qrcode et en
pdf !

Le qrcode
=========

.. image:: {static}/resources/qrcode.png
 :alt: Un qrcode généré
 :align: center
 :width: 50%

Je ne présente plus les qrcode, que l'on rencontre déjà partout. La seule chose
qui est vraiment importante ici, est la taille limite des données qu'il peut
contenir (source_) :

* Caractères numériques : maximum 7 089
* Caractères alphanumériques : maximum 4 296
* Binaires (8-bits) : **maximum 2 953 octets**
* Kanji/Kana : maximum 1 817 caractères

.. _source: https://fr.wikipedia.org/wiki/Code_QR#Sp%C3%A9cification

Ici, c'est la limite de données binaire qui nous intéresse : 2953 octets. Notre
clef GPG dépassant probablement cette limite, nous allons la découper en
plusieurs lots, qui seront chacun d'eux transformés en image.

La commande est assez simple :

.. code-block:: bash

  # Exporte la clef privée et découpe en lot de 2500 octets
  gpg --export-secret-keys --armor | split -C 2500 - splitkey-

(ici nous créons autant de fichier `splitkey` que nécessaire, avec une taille
de 2500 octets chacun).

Ensuite, à l'aide de l'application qrencode, on transforme chaque fichier en
image :

.. code-block:: bash

  # Génère un QRCode pour chacun d'eux
  for file in tmp/splitkey-??; do
      qrencode --size 3 -d 150 -t eps -o "${file}.ps" -r "${file}"
  done

J'utilise ici le format postcript, afin de pouvoir l'intégrer facilement dans
un fichier pdf : étape suivante !

Récupérer les données
=====================

Il suffit pour cela de lire chaque image, puis concaténer tous les fichiers
entre eux. La lecture peut être faite à l'aide du lecteur zbarimg, qui fait
partie du paquet zbar-tool dans debian :

.. code-block:: bash

  for file in *.png; do
    zbarimg -1 -q --raw  ${file};
  done > private.key

  gpg --import private.key

(le plus long dans ce cas, est de scanner les fichiers pour en extraire les
images…)

Le pdf
======

L'idée est ensuite d'assembler tous ces qrcode dans un document pdf que l'on
va ensuite imprimer. Comme je suis un grand amateur de la combinaison rst +
latex, c'est avec ces deux outils que l'on va construire notre document final.

Puisque l'on dispose d'un fichier à imprimer, autant y ajouter une notice, avec
la manière dont les données ont été générées, et comment les récupérer. À
ce moment-là, le pdf devient autoporteur, et il est possible ensuite de
l'oublier puisqu'il contient en lui-même toutes les informations nécessaires.

Voilà un lien pour télécharger un package réalisant tout ça :

.. figure:: {static}/images/mimetypes/package-x-generic.png
    :alt: get the file
    :align: center
    :target: {static}/resources/gpg_export.tar.gz

    Télécharger

la commande `make` permet de lancer les commandes ci-dessus, et générer un
fichier pdf avec les images, et une petite documentation avec les commandes
ci-dessus (voir le fichier `private.rst` qui contient le modèle).

Vous pouvez ensuite imprimer le pdf, et supprimer les fichiers, et tester !

À titre d’exemple, voilà ce que donne `le fichier généré`_

.. image:: {static}/images/chiffer/private.png
 :alt: Un rendu
 :align: center
 :width: 50%
 :target: {static}/images/chiffer/private.pdf

.. _`le fichier généré`: {static}/images/chiffer/private.pdf
