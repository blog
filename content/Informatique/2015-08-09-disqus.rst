.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==============================
Pelican, disqus, et vie privée
==============================

:date: 2015-08-09
:tags: Libre, blog, ViePrivée
:slug: pelican-disqus-vie-privee
:logo: {static}/images/disqus/privacy.jpg
:summary:   Comment configurer pelican pour utiliser les commentaires disqus,
            sans que le service ne vienne systématiquement pister les
            visiteurs…

.. figure:: {static}/images/disqus/privacy_240.jpg
    :figwidth: 240
    :figclass: floatright
    :alt: Privacy !

    Image : `Frankielon`_ (creativecommons_)

.. _Frankielon: https://www.flickr.com/photos/armydre2008/3865993401/
.. _creativecommons: https://creativecommons.org/licenses/by/2.0/

.. admonition:: Article obsolète
  :class: warning

  J’ai fini par désactiver complètement les commentaires sur le blog. Même si
  la solution est toujours pérenne, celle-ci n’est plus active ici.

Le moteur de blog statique propose nativement un plugin pour gérer les
commentaires avec disqus. Il permet de donner un peu de dynamisme au blog, qui
est entièrement statique… Sauf qu'il s'agit d'un service non libre, qui
enregistre les données des utilisateurs…

Le plugin disqus_static_ permet d'intégrer à la page html les commentaires
trouvés sur le service disqus. Cela permet de les inclure nativement dans
l'article, et évite au lecteur du blog d'informer disqus de sa navigation sur
le site. Voici la manière dont je l'utilise sur le blog.

.. _disqus_static: https://github.com/getpelican/pelican-plugins/tree/master/disqus_static

.. admonition:: Attention !

    Cette configuration est *moins pire* que celle proposée par défaut par le
    blog, mais elle continue d'enregistrer l'ensemble des commentaires auprès
    d'une entreprise privée. Mieux vaut, si vous en avez la possibilité,
    basculer sur une solution hébergée comme isso_.

.. _isso: http://posativ.org/isso/

Création du compte
==================

En suivant la documentation du plugin, on enregistre notre application sur le
site de l'`api de disqus`_ :

.. _api de disqus: https://disqus.com/api/applications/

.. image:: {static}/images/disqus/disqus_api.png
    :alt: Configuration du compte

Le compte n'a besoin d'accéder aux commentaires qu'en mode lecture, penser
aussi à renseigner le nom de domaine utilisé par le blog dans les paramètres du
compte.

Configuration du plugin
=======================

Le compte et la clef seront renseignés dans un fichier à part, que nous allons
nommer `disqus.py`. Normalement, il devrait juste contenir ces lignes :

.. code-block:: python

    #!/usr/bin/env python
    # -*- coding: utf-8 -*-

    # Configuration des commentaires
    DISQUS_SITENAME='Votre compte disqus'
    DISQUS_SECRET_KEY = u'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    DISQUS_PUBLIC_KEY = u'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'


Pour éviter de diffuser ce fichier sur un serveur git public, ne pas oublier de
l'ajouter au fichier .gitignore ! Ainsi, nous serons sûr que notre clef privée
ne sera jamais transmise sur internet…

Dans le fichier `publish.conf`, nous allons ajouter le chargement du plugin :

.. code-block:: python

    import sys
    import os.path
    try:
        sys.path.append(os.path.dirname(os.path.abspath(__file__)))
        from disqus import *
        PLUGINS.append("disqus_static")
    except:
        pass

Cela permet de génerer le blog, même si la configuration pour les commentaires
n'est pas présente.

Utiliser le fichier `publish.conf` permet de générer le blog sans faire appel à
l'API pour éditer le blog, par contre, ceux-ci seront intégrés dès que l'on
lancera la commande `make publish` pour publier le blog.

Changement du template
======================

Maintenant que tout est prêt, il ne reste plus qu'à modifier son template pour
éviter de charger disqus automatiquement. C'est dommage d'intégrer les
commentaires dans la page, si ceux chargés par disqus sont affichés
automatiquement…

Sur mon blog, il faut cliquer sur le bouton pour recharger les commentaires (ou
pour en ajouter de nouveaux). C'est juste un peu de javascript, et vous pouvez
vous inspirer de ce bout de code :

.. code-block:: html

    {% if DISQUS_SITENAME %}

    <div class="comments">
    <h2>Commentaires&nbsp;:</h2>
        <div id="disqus_thread">
        {% if article.disqus_comments %}
        <ul class="post-list">
            {% for comment in article.disqus_comments recursive %}
            <li class="post">
                <div class="post-content">
                    <div class="avatar hovercard">
                        <img alt="Avatar" src="{{ comment.author.avatar.small.cache }}">
                    </div>
                    <div class="post-body">
                        <header>
                            <span class="publisher-anchor-color">{{ comment.author.name }}</span>
                            <span class="time-ago" title="{{ comment.createdAt }}">{{ comment.createdAt }}</span>
                        </header>
                        <div class="post-message-container">
                            <div class="post-message publisher-anchor-color ">
                                {{ comment.message }}
                            </div>
                        </div>
                    </div>
                </div>
                {% if comment.children %}
                <ul class="children">
                    {{ loop(comment.children) }}
                </ul>
                {% endif %}
            </li>
            {% endfor %}
        </ul>
        {% else %}
          Aucun commentaire pour l'instant.
        {% endif %}
        </div>
    </div>
    <div id="disqus_comments">
        <button onclick="load_disqus()">recharger</button>
    </div>
    <script type="text/javascript">
        var disqus_identifier = "{{ article.url }}";
        function load_disqus() {
            var dsq = document.createElement('script');
            dsq.type = 'text/javascript';
            dsq.async = true;
            dsq.src = '//{{ DISQUS_SITENAME }}.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            var load_button = document.getElementById('disqus_comments');
            load_button.parentNode.removeChild(load_button);
      };
    </script>
    {% endif %}

