.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

=====================
Encore des cvs en rst
=====================

:date: 2020-12-03
:tags: latex, restructuredText
:summary: |summary|


.. default-role:: literal

|summary|

.. |summary| replace::

  J'avais déjà présenté comment modifier_ le format rst pour produire un cv avec
  la classe moderncv de latex. Cette fois je propose de se créer soi-même son
  modèle tout en gardant le format RST.

.. _modifier: {filename}2015-10-03-moderncv.rst


L'idée est de conserver le standard du format rst et de modifier les fichiers
latex pour les adapter en fonction du besoin. Cela signifie que le même fichier
source peut être utilisé avec les différents exemples sans changements (ou
presque).

Le résultat
===========

Commençons directement par le final, voilà le résultat :

.. image:: {static}/images/cv/model5.png
 :width: 50%
 :class: floatleft
 :alt: Exemple de rendu

le code est disponible sur mon `dépôt git`_ si vous souhaitez le cloner et
récupérer les sources.

.. _dépôt git: http://git.chimrod.com/latex_template.git/

Les directives RST
==================

La sidebar
----------

.. code-block:: rst

  .. sidebar:: Titre

   Texte à renseigner dans le bandeau

Permet de créer du texte dans la barre latérale. Le texte peut être une liste,
une image ou tout autre élément standard. Il faut juste faire attention à la
largeur qui oblige à rester concis…


.. admonition:: Sans titre ?
 :class: hint

 rst oblige à déclarer un titre dans la directive, ce qui n'est pas forcément
 ce que l'on souhaite. Par contre le titre peut être vide ! Il suffit
 d'échapper une espace :


 .. code-block:: rst

  .. sidebar:: \

    .. image:: mon profil.jpg
      :width: 100%

Les listes
----------

Avec la directive `class`, il est possible d'appliquer un style qui sera
présent jusqu'à la fin du paragraphe. Ce style est associé à une commande Latex
qui permet l'appliquer le style en question. Ainsi, avec la classe `checklist`,
toutes les puces des listes sont remplacées par une coche :

.. code-block:: rst

  .. class:: checklist

  * 1er élément
  * Second élément

La commande latex suivante est utilisée :

.. code-block:: latex

  \newenvironment{DUCLASSchecklist}{\renewcommand{\labelitemi}{\faCheck}}{}

Il est facile de modifier la commande pour charger d'autres types de marques,
voir le fichier `10_lists.tex`_ pour la liste complète.

.. _10_lists.tex: http://git.chimrod.com/latex_template.git/tree/common/10_lists.tex

Il est possible de modifier le style de liste en cours de route, mais il faut
faire attention à appeler la commande **avant** la ligne que l'on souhaite
modifier. C'est pourquoi l'on retrouve l'exemple suivant dans lequel on annonce
les icônes avant la ligne suivante :

.. code-block:: rst

  .. sidebar:: Informations

    * 75001 Paris |phoneicon|
    * 01 23 45 67 89 |mailicon|
    * email@example |noicon|

`|phoneicon|` est en fait une substitution de texte qui insère une commande
latex à la place (et donc ne substitue rien du tout :))

Les barres de niveau
====================

Autre point assez amusant, il est possible de faire des graphiques à partir de
données dans le fichier. Les niveaux représentés sous forme d'étoiles ou de
barres sont construits dans latex à partir des lignes suivantes :

.. code-block:: rst

 .. role:: level
 .. role:: levelbox
 .. role:: star

 :star:`4.5`
 :levelbox:`5`
 :level:`3`

Il s'agit de rôles personnalisés, qui sont interprétés comme des commandes
latex prenant un paramètre : la valeur du niveau. Celui-ci est ensuite
représenté sous forme d'étoile colorées, ou barre de niveau. On peut imaginer
toutes les fantaisies possibles…

Autres éléments
===============

Il n'y en a pas d'autres. Tout le reste est ensuite standard et suit les
directives rst. (La directive `admonition`_ permet également des
personnalisations assez avancées, mais je ne l'ai pas appliqué ici).

.. _admonition: https://docutils.sourceforge.io/docs/ref/rst/directives.html#admonitions

L'organisation
==============

Chaque répertoire contient un modèle différent, et tous les modèles suivent le
même schéma :

::

  |
  |\ Makefile
  |\ fichier.rst (le fichier source à traiter)
  \ resources (le répertoire contenant les modèles latex)
    |
    |\ 00_colors.tex (la définition des couleurs)
    |\ 00_length.tex (les longueurs pour les marges ou autres paramètres)
    |\ 00_preamble.tex (le fichier contenant les paramètres principaux)
    |\ 10_fonts.tex (les polices utilisées dans le document)
    |\ 10_title_style.tex (la personnalisation des titres)
     \ autres directives latex

Tous les fichiers qui sont présents dans le répertoire `resources` sont
automatiquement chargés, et les modules présents dans le fichier `modules` sont
également chargés.

Pour la simplification du dépôt, des fichiers communs à tous mes styles sont
présents dans le répertoire `common`

Et voilà
========

J'espère que ces petits exemples vous donneront des idées, et qu'ils vous
permettront de construire vos propres modèles. L'idée est vraiment de laisser
toute la mécanique latex en arrière-plan, et se simplifier au maximum
l'écriture !
