.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==========================
Lire ses podcasts avec MPD
==========================

:date: 2024-06-19
:tags: MPD

J’aime beaucoup MPD_. Il s’agit d’une application pour gérer sa musique qui est
conçue pour séparer le lecteur audio de l’interface pour le contrôler. Je
l’utilise à la maison sur un petit rasperry pi zero (voir `mon article sur la
configuration de la carte`_), connecté en wifi, et je peux le contrôler via le
téléphone, sans avoir besoin d’allumer le PC du bureau.

.. image:: {static}/images/buildroot/connexion.jpeg
    :alt: Le serveur de son
    :align: center

MPD est capable de lire un flux réseau, par exemple une web radio, si on lui
donne l’url, ce qui en fait une solution économe, puisqu’elle permet de se
passer d’un navigateur, et peut être démarrée tout simplement n’importe quel
téléphone. Mais je voulais aller plus loin et lire les podcasts également.

Prenons `une émission diffusée sur FIP
<https://www.radiofrance.fr/fip/podcasts/fip-tape>`_ et dont les épisodes sont
disponibles en podcast :

.. image:: {static}/images/mpd/fip-tape.webp
    :alt: Fip-tape

En récupérant l’URL du flux RSS, on peut charger les épisodes dans la playlist
avec la ligne de commande suivante :

.. code:: console

   mpc load https://radiofrance-podcast.net/podcast09/rss_24684.xml

::

   Playlist Commands
       load <file> - Loads <file> as queue.  The option
              --range may be used to load only a portion of the file

       lsplaylists: - Lists available playlists.

       playlist [<playlist>] - Lists all songs in <playlist>. If
              no <playlist> is specified, lists all songs in the current queue.

       rm <file> - Deletes a specific playlist.

       save <file> - Saves playlist as <file>.

MPD va lire le flux RSS, charger le titre des épisodes, et les associer avec
les fichiers en ligne pour chacun d’eux. Le problème est qu’il s’agit de la
playlist courante, et il faut répêter cette manipulation à chaque fois que l’on
souhaite écouter l’un des épisodes. Il faut un ordinateur allumé, ouvrir un
terminal, ce qui est loin d’être pratique !

Il existe un moyen de sauvegarder la playlist, avec la commande `save`, mais la
playlist ne contient plus que les fichiers à lire, et le titre du podcast est
perdu. Cela crée une playlist incompréhensible ce qui ne nous aide pas vraiment.

Au lieu de ça, nous allons demander à MPD de nous génerer la playlist au format
m3u_, il s’agit d’un fomat très simple, constitué d’un simple fichier texte. La
commande `playlist` prend en effet un paramètre `--format` qui permet de
spécifier comment la playlist courante doit être affichée :

.. code:: console

    $ mpc -f "##EXTINF:0,%title%\n%file%" playlist
    #EXTINF:0,La mixtape de Françoise Hardy
    https://rf.proxycast.org/9b541821-7758-4634-88ae-b1fbb9f153b5/24684-12.06.2024-ITEMA_23771367-2024Y52935S0164-21.mp3
    #EXTINF:0,Tout sur ma mère
    https://rf.proxycast.org/f2d90d91-cc4d-4ea5-9e21-50bf138783ba/24684-25.05.2024-ITEMA_23752523-2024Y52935S0146-21.mp3
    #EXTINF:0,La mixtape d'Amy Winehouse
    https://rf.proxycast.org/fca9204c-caea-4292-b45d-e31b77857329/24684-02.05.2024-ITEMA_23728745-2024Y52935S0123-21.mp3

Ici, nous affichons la sortie sur deux lignes (le `\n` correspond au séparateur
de ligne), sur la première le titre du morceau, et sur la seconde le lien vers
le moreau à jouer. Et cela tombe bien, car MPD est capable de lire les fichiers
M3U_ ! On peut donc enregistrer la sortie de la commande dans un fichier, et
déposer ce fichier dans le répertoire des playlists.

Et comme l’ensemble des commandes peut se faire sans aucune action, on peut
automatiser la mise à jour des fichiers pour détecter les nouveaux épisodes et
retirer ceux qui ne sont plus disponibles.

Cela nous donne le script suivant:

.. code:: bash

    #!/bin/sh
    # Update the mpd playlist from RSS fields

    playlist_directory="/home/playlists"

    update_playlist () {
        mpc load $2
        echo "#EXTM3U" > "${playlist_directory}/Podcast - $1.m3u"
        mpc -f "##EXTINF:0,%title%\n%file%" playlist >> "${playlist_directory}/Podcast - $1.m3u"
        mpc clear
    }

    case $(mpc status "%state%") in
        paused)
            mpc clear
            update_playlist "Fip Tape" "https://radiofrance-podcast.net/podcast09/rss_24684.xml"
            ;;
        *)
    esac

Et voilà, il ne reste qu’à compléter le fichier avec l’ensemble des podcasts
que l’on souhaite suivre, et le mettre dans une tâche planifiée pour
l’automatiser.

Bonne musique !

.. _MPD: https://www.musicpd.org/
.. _`mon article sur la configuration de la carte`: {filename}2021-11-piaudio.rst
.. _m3u: https://fr.wikipedia.org/wiki/M3U
