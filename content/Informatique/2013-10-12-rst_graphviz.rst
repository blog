.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Ajouter graphviz dans les documents restructuredText
####################################################

:date: 2013-10-12
:tags: Libre, reStructuredText, graphviz
:logo: /images/graphviz/graphe.png
:summary: |summary|

.. default-role:: literal

:2014-05-05:    Je met l'article à jour avec la dernière version du script que
                j'utilise. J'en profite pour donner un peu plus d'exemples.

.. image:: {static}/images/graphviz/graphe.png
    :class: floatleft
    :scale: 50
    :alt: Graphe

|summary|

.. |summary| replace::
    C'est en utilisant un produit tous les jours que nous vient l'envie de
    l'améliorer. Dernièrement, j'ai eu besoin de générer un texte contenant des
    graphes. Il y a quelques temps, j'aurais préparé mes graphes dans des
    images à part, et inclus les images dans mon document, sauf que cette fois,
    j'ai décidé d'inclure directement le code du graphe dans mon document.

Graphviz_ est une merveilleuse librairie pour générer des représentations de
graphes comme celle qui se trouve à côté. À partir d'un fichier texte contenant
la description du graphe, l'application nous génère une image (avec différents
formats possibles) du graphe. Par exemple, l'image ci contre peut-être
représentée avec le code suivant :

.. code-block:: dot

    digraph G {

        a -> a
        a -> b
        d -> b
        b -> c
        c -> b
    }

.. _Graphviz: http://graphviz.org/

Je ne rentre pas davantage dans la description de gv, l'application est très
complète, et un article ne suffirait pas à tout couvrir !

La question qui se pose est donc de pouvoir inclure directement graphviz lors
de la génération du document. Pour ça, j'ai créé une nouvelle directive_,
`graphviz` qui appele directement dot lors de la génération du document.

.. _directive: http://docutils.sourceforge.net/docs/ref/rst/directives.html

.. list-table::
    :header-rows: 1
    :stub-columns: 1
    :widths:    10 45 45

    *   - Exemple
        - Code
        - Résultat
    *   - Insérer un graphe orienté
        -
            .. code-block:: rst

                .. graphviz:: digraph

                    a -> a
                    a -> b
                    d -> b
                    b -> c
                    c -> b

                    ..
                    ceci est une légende
        -

            .. figure:: {static}/images/graphviz/graphe.png
                :alt: graphe orienté avec légende

                ceci est une légende

    *   - Insérer un graphe non-orienté
        -
            .. code-block:: rst

                .. graphviz:: graph

                    a -- a
                    a -- b
                    d -- b
                    b -- c
                    c -- b
        -

            .. figure:: {static}/images/graphviz/no.png
                :alt: graphe non orienté

    *   - Utiliser des options
        -
            .. code-block:: rst

                .. graphviz:: digraph

                    rankdir = LR;

                    a -> a
                    a -> b
                    d -> b
                    b -> c
                    c -> b
        -

            .. figure:: {static}/images/graphviz/options.png
                :alt:   graphe horizontal


Pour ceux que ça intéresse, voici le script pour rst2html_ et rst2latex_. Le
code est similaire, cela ajoute une nouvelle directive qui génère le document à
l'aide de graphviz, et stocke l'image dans un fichier temporaire, créé dans un
répertoire `tmp` (qui doit exister avant de lancer la commande). On pourrait
très facilement l'ajouter à rst2odt en suivant le même principe.

Le script rst2latex génère les images en pdf, il est prévu pour être utilisé
avec `pdflatex`.

C'est tout, le langage est tellement simple que ça serait dommage de ne pas en
profiter !

.. _rst2html: {static}/resources/rst_graphviz/rst2html.py
.. _rst2latex: {static}/resources/rst_graphviz/rst2latex.py
