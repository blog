.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

CS Unplugged
============

:date: 2013-06-08
:tags: Libre, Livre
:summary: |summary|
:logo: /images/unplugged/unplugged.jpg

.. image:: {static}/images/unplugged/together.gif
    :width: 320
    :class: floatright
    :alt: Together with computer

Il y a quelques années, j'avais découvert le livre `Computer Science
Unplugged`. La semaine dernière, je suis retombé sur le pdf, et l'ai relu avec
le même intérêt. D'ou cet article pour faire partager cette (re)découverte.

|summary|

.. |summary| replace::
    `CS Unplugged`_ est un livre pour faire découvrir l'informatique, sans
    ordinateur.  Il s'agit d'un support pédagogique, qui explique de manière
    très claire comment fonctionne l'informatique, le format des données que
    traite l'ordinateur, et comment celui-ci fait pour traiter les
    informations.

.. _CS Unplugged: http://csunplugged.org/

Je n'ai pas de relations avec le monde enseignant, et ne me tiens pas au
courant de la manière dont l'informatique est traitée en classe. Pourtant je
trouve que ce support de cours est intéressant à plus d'un titre :

* tout d'abord il est libre ! C'est suffisamment rare pour mériter d'être
  souligné. Le livre est diffusé sous licence creativecommons et les sources
  (au format .doc) sont `téléchargeable sur le site`_.

* ensuite il est vraiment très pédagogique. Je ne me suis pas ennuyé en le
  lisant, et les exemples sont vraiment bien introduits.

.. _téléchargeable sur le site: http://csunplugged.org/open-source-edition-ms-word

.. image:: {static}/images/unplugged/parity.jpg
    :class: floatleft
    :alt: Together with computer

L'exemple ci-contre est une explication des bits de parités et du contrôle des
erreurs. Comme l'ensemble des activités, cela se fait avec des cartes ou des
images, et le but derrière l'activité est de comprendre comment l'ordinateur
fonctionne. À la lecture de cette image, avez-vous compris comment la fille a
pu deviner quelle carte avait été retournée ? (Je vous ai donné un indice en
parlant de bits de parité ; imaginez que chaque carte représente un bit). Si
vous ne trouvez pas, allez-voir l'activité 4 à la page n°31 !

Chaque activité est accompagnée d'un peu de théorie, ainsi que d'exemples issus
de la vie courante (ici l'utilisation des sommes de contrôles dans le n°ISBN
des livres).

Les activités vont à chaque fois un peu plus loin dans la complexité, mais tout
en restant facilement abordable ; à la fin du livre, vous serez même capable
d'expliquer le principe du `tri rapide`_ !

.. _tri rapide: https://fr.wikipedia.org/wiki/Tri_rapide

Si vous avez besoin de faire découvrir l'informatique, où si vous êtes à la
recherche d'une pédagogie qui tourne autour de l'ordinateur, de la théorie de
l'information et des algorithmes, vous trouverez probablement votre bonheur
dans ce livre — la page de couverture parle d'activité d'éveil à partir de
l'école primaire, mais sans limitation d'âge, et ce serait dommage de passer à
côté de cette pépite…
