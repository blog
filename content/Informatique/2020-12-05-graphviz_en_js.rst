.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

===========================
Prise de note avec graphviz
===========================

:date: 2020-12-06
:tags: graphviz, javascript
:summary: |summary|

.. default-role:: literal

.. |summary| replace::


 J'utilise Graphviz depuis un moment — même pour des notes — car la
 représentation en graphe permet de synthétiser facilement des notions
 complexes. Mais c'est compliqué et long à saisir, du coup, j'ai choisi de me
 faire un petit outil pour me faciliter la vie…

J'utilise Graphviz depuis un moment — même pour des notes — car la
représentation en graphe permet de synthétiser facilement des notions complexes :

- relation de dépendances
- séquence de processus
- …

Mais il me manquait une manière de pouvoir les créer de manière rapide,
c'est-à-dire avec une syntaxe adaptée à la prise de note, et pouvoir éditer le
résultat en temps réel. Or créer une syntaxe est quelque chose que je sais
faire, et voir le résultat en temps réel est possible grâce à `viz.js`_ qui a
recompilé Graphviz en javascript à l'aide d'Emscripten_, et permet donc de le
faire tourner dans le navigateur.


.. _viz.js: http://viz-js.com/
.. _emscripten: https://fr.wikipedia.org/wiki/Emscripten

Il ne reste donc plus qu'à assembler tout ça, pour faire tourner un petit
éditeur de graphe. J'ai choisi de ne représenter que des tables, et d'utiliser
l'indentation pour marquer la relation au parent : (si vous souhaitez en
profiter en pleine page, c'est `ici que ça se passe`_)

.. _ici que ça se passe: {filename}/pages/graph-editor/graph-editor.rst#graph-editor

La démo
-------

.. raw:: html

    <style>
    #app {width: 100%; height: 100%; overflow: hidden; position: relative;}
    #panes {display: flex; width: 100%; height: 100%; overflow: auto;}
    #output svg, #output object { top: 0; left: 0; width: 100%; height: 100%; }
    .flex-child {flex: 1;}
    .vcenter {display: table-cell; vertical-align: middle}
    #overlay {
    display: table;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.5);
    z-index: 99;
    color: white;
    text-align: center;
    }
    </style>

    <div id="app" disabled="true">
      <button id="dot_output">Export DOT</button>
      <button id="png_output">Export PNG</button>
      <div id="panes">
        <textarea id="editor" class="flex-child">
    g1
     Nom
     -
     propriété 1 <-> g2:2
     propriété 2

    g2
     Autre
     -
     propriété 1
     encore une autre <i>entrée</i>
     autre champ -> g1:3 retour

    g3
     Dernier élément
     -
     ligne 2
     ligne 3
    </textarea>
      <div id="graph" class="flex-child">
        <div id="output">
          <div id="svg"><object data="{static}/resources/viz.js/example.svg" type="image/svg+xml"></object></div>
            <div id="error"></div>
          </div>
        </div>
      </div>
      <div id="overlay">
      </div>
    </div>


.. admonition:: Exécution du script
 :class: hint
 :name: note

 Les librairies étant relativement lourdes (+2Mo), elles ne sont pas chargées
 automatiquement avec la page. Je vous laisse cliquer sur le bouton pour les
 charger et lancer la mise à jour.

 .. raw:: html

  <div style="text-align:center; width:100%; position:relative;">
  <button type="button" id="load_button">Activer</button>
  </div>

  <script type="text/javascript">
   async function getScript(url){
     var script = document.createElement('script');

     promise = new Promise((res, rej) => {
       script.addEventListener('load', () => {
         res(script);
       })
     });

     script.setAttribute('type', 'text/javascript');
     script.setAttribute('src', url);
     if(document.body == null) {
       document.head.appendChild(script);
     } else {
       document.body.appendChild(script);
     }
     return promise;
   }
  var load_button = document.getElementById('load_button');

  load_button.onclick = async function() {
      var note = document.getElementById('note');
      note.remove();
      var s1 = getScript("/resources/viz.js/convert.js");
      await Promise.all([s1]);
      generator.load();
      var overlay = document.getElementById("overlay");
      overlay.remove();
      return false;
  }
  </script>



Vous pouvez édite le texte ci-dessus pour mettre à jour le graphe de la partie
droite directement. Les nœuds sont représentés à l'aide des `HTML Like label`_
de graphviz, il est donc possible d'utiliser de la couleur, ou des balises de
mises en formes qui seront reprises dans le graphe final. Tout ce qui est saisi
est transformé dans le navigateur, et aucune donnée ne transite sur le réseau.


.. _HTML Like label: https://www.graphviz.org/doc/info/shapes.html#html

Avec les boutons d'export, cela me suffit pour produire un petit schéma
rapidement, ou ensuite le retravailler le fichier dot correspondant si j'ai
besoin d'aller plus loin…

Le code
-------

Il est écrit en OCaml et compilé en javascript (c'est pour ça qu'il s'exécute
dans la page). Vous pouvez télécharger une version hors ligne à exécuter en
local ci-dessous :

.. figure:: {static}/images/mimetypes/package-x-generic.png
    :alt: get the file
    :align: center
    :target: {static}/resources/viz.js/graphviz.zip

    Télécharger

la commande `make html` permet de régénérer les sources.
