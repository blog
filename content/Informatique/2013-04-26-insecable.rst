.. -*- rst -*-
.. -*-  coding: utf-8 -*-

L'espace fine insécable en HTML
===============================

:date: 2013-04-26
:tags: Python, reStructuredText

La typographie est bien souvent malmenée dès que l'on utilise le web comme
support. Pourtant, `les règles sont simples`_, et il est facile de les
appliquer quand on publie un document, à partir du moment ou cela ne se fait
pas à la main. Pour ma part, j'utilise le plugin typogriphy_ pour faire le
rendu des articles de ce blog. Ce plugin permet de convertir le texte écrit
dans un éditeur de texte brut en un texte destiné à être affiché dans une page
html :

* Substitution des quotes et doubles quotes par des apostrophes et
  guillemets
* Écriture en capitales des mots composés de majuscules
* Ajout d'espaces insécables pour éviter qu'un mot se retrouve seul sur une
  ligne en fin de paragraphe.

Un exemple de rendu est disponible sur le `site du projet`_.

.. _les règles sont simples: http://edu.ca.edu/typo/
.. _typogriphy:     https://github.com/mintchaos/typogrify
.. _site du projet: http://static.mintchaos.com/projects/typogrify/

Ainsi, je n'ai pas à me soucier de la mise en page lors de la rédaction des
articles, la transformation se fait sur le texte html généré, cela permet de se
concentrer sur le contenu et non la mise en forme.

On peut croire que ça n'est que du gadget et est complètement inutile, mais le
but de la typographie n'est pas de faire joli (même si l'esthétique à sa place,
bien sûr), le but est avant tout la lisibilité.

Le problème
-----------

Par contre, le plugin ne gère pas la langue française, dans le sens où il
n'insère pas d'`espace fine insécable`_ quand c'est nécessaire :

* après les signe de ponctuation double (« |nbsp| ; |nbsp| : |nbsp| ? |nbsp| !
  |nbsp| »)
* entre les guillemets et le texte qu'ils contiennent
* séparateur de chiffre lors des numéros de téléphones…

Quitte à disposer d'un plugin qui s'occupe de la mise en page, autant
l'utiliser jusq'au bout ; il est possible de modifier le plugin pour insérer ce
caractère quand il faut, mais beaucoup de polices n'intègrent pas ce caractère
dans leur table, utiliser le caractère unicode NNBSP_ peut donc causer des
problèmes si le navigateur ne gère pas le cas d'erreur. Ce défaut à d'ailleurs
poussé OpenOffice_ à ne pas l'utiliser dans les textes.

Vous pouvez malgré tout utiliser le caractère NNBSP si vous êtes sûrs que la
police contient ce caractère dans sa table. Si ça n'est pas le cas, vous
risquez d'avoir des problèmes d'affichage. À noter que les versions d'IE8 et
les précedentes, qui ne gèrent pas correctement ce caractère, `sont encore
utilisées`_ par 6 % des utiliseurs, encore quelque temps et le problème ne
devrait plus se poser.

.. _espace fine insécable: http://fr.wikipedia.org/wiki/Espace_fine_ins%C3%A9cable
.. _NNBSP: http://www.fileformat.info/info/unicode/char/202f/index.htm
.. _OpenOffice: http://wiki.openoffice.org/wiki/Non_Breaking_Spaces_Before_Punctuation_In_French_%28espaces_ins%C3%A9cables%29#Exclusion_of_the_NARROW_NO-BREAK_SPACE_.28U.2B202F.29
.. _sont encore utilisées: http://www.w3schools.com/browsers/browsers_explorer.asp

Les solutions
-------------

S'il n'est pas possible d'utiliser un caractère unicode pour notre rendu, on
peut l'accompagner d'indication de mise en page pour obtenir ce que l'on
souhaite. Il y a même plusieurs solutions qui peuvent s'appliquer, elles sont
présentées sur stackoverflow_ — les problèmes sont généralement communs à
plusieurs personnes…

.. _stackoverflow: http://stackoverflow.com/questions/595365/how-to-render-narrow-non-breaking-spaces-in-html-for-windows

Espace insécable
~~~~~~~~~~~~~~~~

Cette espace là est reconnue et est utilisée partout ! Toutefois, si l'on a
pris l'habitude de lire du texte respectant la typographie française, on risque
d'être surpris par le texte généré (c'est pourtant la solution qui a été
retenue par OpenOffice), les signes de ponctuation donneront l'impression
d'être décolés du texte qu'ils accompagent.

Il est possible de l'utiliser, mais en utilisant un attribut html pour en
réduire la taille. C'est possible avec l'attribut FONT-SIZE. Par contre, on
mélange le contenu et l'apparence : ne pas séparer le texte de la ponctuation
fait partie de la mise en page, pas du sens donné au texte.

Espace fine
~~~~~~~~~~~

C'est l'autre solution : cette espace à la même largeur qu'une espace fine
insécable, mais risque de séparer le caractère du signe de ponctuation qui
l'accompage, pouvant laisser le texte sur une fin de ligne et le signe de
ponctuation en début de la ligne suivante… Par contre, on peut spécifier dans
les attributs que celle-ci ne doit pas être coupés. C'est faisable avec
l'attribut HTML NOWRAP_. Cela est pris en charge par la majorité des
navigateurs et permet d'obtenir le rendu voulu. Si l'on fait un copier-coller
du texte html dans un logiciel de traitement de texte, l'espacement sera
correctement préservé.

.. _NOWRAP: http://www.w3schools.com/tags/att_td_nowrap.asp

Le correctif
------------

J'ai retenu la seconde solution, et ai créé une branche github_ pour cette
version, que j'utilise actuellement pour le blog.

Tout d'abord, le texte n'est substitué qu'à l'intérieur d'une balise HTML, les
attributs ne sont pas pris en compte, ça évite de casser l'affichage en allant
trop loin dans le traitement.

Ensuite, étant donné qu'il s'agit de règle de typographie propre à la langue
française, cette substitution ne s'applique que dans le cas où la locale est
fixée à `fr_FR`, ce qui laisse le champ libre pour d'autres implémentations.

Le remplacement (aucune espace n'est ajoutée, on ne fait que des substitutions)
se fait dans les conditions suivantes :

* avant les signes de ponctuation double,
* entre les guillemets et le texte qu'ils contiennent,
* entre un chiffre et le symbole de pourcentage,
* entre deux chiffres.

.. _github: https://github.com/Chimrod/typogrify

Et voilà : on dispose ainsi d'un code html qui est déjà beaucoup plus soigné !
On n'arrive pas à la cheville d'un pdf généré avec latex — il ne s'agit que
d'un blog — mais ça permet d'avoir déjà amélioré la présentation de son texte,
et de s'être un peu penché sur les problèmes de typographie…

.. |nbsp| unicode:: 0xA0
   :trim:

