.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Guide d'utilisation de rst2odt
==============================

:date: 2013-05-07
:tags: Libre, reStructuredText
:summary: |summary|
:logo: /images/rstodt/writing_75.jpg


.. figure:: {static}/images/rstodt/writing.jpg
    :figwidth: 150
    :figclass: floatright
    :alt: Bureau

    Image : `AJ Cann`_ (creativecommons_)

.. _creativecommons: http://creativecommons.org/licenses/by-nc-sa/2.0/deed.fr


|summary|


.. |summary| replace::
    J'ai déjà souvent abordé le langage reStructuredText_ dans mes articles, et
    je l'utilise fréquement, que ce soit pour écrire sur ce blog ou publier
    d'autres contenus. On présente souvent rst2pdf_ pour la publication, mais
    il est également possible de générer une sortie vers libreoffice,
    permettant de créer rapidement des documents avec une mise en page
    maîtrisée.

.. _reStructuredText: http://docutils.sourceforge.net/docs/ref/rst/introduction.html
.. _rst2pdf: http://rst2pdf.ralsina.com.ar/

Premiers pas
------------

On commence par installer les outils nécessaires :

.. code-block:: console

    $ sudo aptitude install rst2odt

et on lance la génération de la manière suivante :

.. code-block:: console

    $ rst2odt document.rst document.odt

On peut tout de suite consulter le fichier généré pour voir le résultat. Le
rendu est moins aguicheur que celui généré avec rst2pdf, mais il est possible
de changer ça.

Environnement
-------------

Plutôt que de lancer la commande à chaque fois, il est préférable de se créer
un environnement qui va nous servir chaque fois que nous aurons besoin de
publier un nouveau document.

On va se créer un Makefile pour automatiser la génération du document :

.. code-block:: makefile

    resources/template.odt: resources/template.rst
    	rst2odt resources/template.rst resources/template.odt

    %.odt: %.rst resources/template.odt
    	rst2odt --create-links --strip-comments --stylesheet=resources/template.odt --add-syntax-highlight  $< $@
        cp $@ resources/templates.odt

Ainsi, il suffira de lancer la commande `make document.odt` pour générer la
sortie à partir du fichier `document.rst` (`document` est bien sûr à adapter en
fonction du nom du fichier sur lequel vous travaillez).

J'ai créé une archive_ qu'il vous suffit de décompresser pour pouvoir
commencer à travailler ; elle comprend le fichier `makeFile` présenté ci-dessus,
ainsi qu'un template contenant des éléments de base.

.. _archive: {static}/resources/rstodt/rst.zip

Utiliser les templates
----------------------

L'avantage d'utiliser un format balisé est que vous n'avez pas vous soucier du
formattage. Le document généré est prêt à être mis en page, vous n'avez plus
qu'à travailler sur le modèle.

La commande `make` a mis à jour le fichier modèle à partir de votre document de
travail. Vous pouvez donc maintenant modifier les styles à travers libreoffice,
ils seront automatiquement intégrés dans vos prochaines étapes de génération.

rst2odt applique ses propres styles au document, ceux-ci héritant des styles
par défaut d'OpenOffice, vous pouvez donc utiliser vos template habituels pour
la mise en forme de votre document. La liste est présentée sur la
documentation_ de l'application.

En séparant le contenu de l'application, on peut ainsi facilement réutiliser un
style d'un document à un autre, on se décharge de la mise en page pendant tout
le temps de la rédaction.

.. _documentation: http://docutils.sourceforge.net/docs/user/odt.html

Aller plus loin
---------------

Le langage reStructuredText utilise des directives_ pour mettre en forme le
texte. Toutes les applications ne les gèrent pas correctement, et certains
outils ont décidé de rajouter leurs propres directives ; bref c'est un peu le
bordel.

La plupart des directives sont correctement prises en compte :

* les tableaux
* les images
* les notes de bas de page (À l'exception près qu'il est impossible de référencer une note de bas de page dans une autre note.)
* la table des matières
* les rôles

Cela devrait répondre à 90 % des usages courants. J'ai déjà présenté un
correctif pour `ajouter la coloration syntaxique`_ dans notre document, et
ainsi utiliser les mêmes directives que rst2pdf_ pour insérer du
code, voici la liste de quelques directives propres à rst2odt :

:meta_:         Permet de renseigner les méta-données dans le document.

Et d'autres qui sont plus ou moins bien suportées :

:container_:    Permet d'appliquer un style particulier sur un bloc de texte.

:raw_:          Pour insérer du code xml au format openDocument.


Au final
--------

C'est bien pratique de pouvoir écrire son document dans un bloc note, sur une
machine plutôt poussive, et malgré ça bénéficier de la force de mise en page de
LibreOffice. On s'affranchit de la plate-forme sur laquelle on travaille (on
peut éditer son fichier sous gedit sur linux, puis sur le bloc-note de
windows, et le reprendre dans un terminal via vim), et lancer la transformation
une fois le texte écrit

Vient alors la phase de mise en page, mais on est alors aidé par l'outil de
traitement de texte pour ça, et surtout, par le fait que le document généré
inclu des styles sur sa totalité, c'est donc tout de suite beaucoup plus
facile, et se fait très rapidement.

Pour ma part, c'est une solution que j'utilise et recommande, et qui au final
donne de meilleurs résultats que rst2pdf.


.. _directives: http://docutils.sourceforge.net/docs/ref/rst/directives.html

.. _meta: http://docutils.sourceforge.net/docs/user/odt.html#the-meta-directive
.. _table : http://docutils.sourceforge.net/docs/user/odt.html#the-table-directive
.. _container: http://docutils.sourceforge.net/docs/user/odt.html#the-container-directive
.. _raw: http://docutils.sourceforge.net/docs/user/odt.html#the-raw-directive

.. _ajouter la coloration syntaxique: {filename}2012-08-18-rstodt.rst

.. _AJ Cann: http://www.flickr.com/photos/ajc1/3531532114/in/photostream/
