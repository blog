.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

==============================================
Se créer un thème de couleur dans son terminal
==============================================

:date: 2014-05-18
:tags: libre, DIY
:logo: /images/palette_75.jpg
:summary:
    Quand on utilise un terminal tous les jours, on a envie de l'adapter en
    fonction de ses goûts. Il existe beaucoup de thèmes disponibles sur le net
    qui permettent de le configurer à sa guise, mais rien qui n'explique
    comment en créer un nouveau.

.. figure:: {static}/images/palette_150.jpg
    :figwidth: 150
    :figclass: floatleft
    :alt: Palette

    Image : `Hafsa Nabeel`_ (creativecommons_)

.. _Hafsa Nabeel: https://www.flickr.com/photos/hafsacreations/3241957057/
.. _creativecommons: http://creativecommons.org/licenses/by-nc-nd/2.0/

Il est assez facile de se créer un thème de couleur pour son terminal. La
plupart du temps on trouve des thèmes à copier qui contiennent la liste des
couleurs à utiliser, mais il est rarement expliqué comment faire pour en créer
un nouveau. C'est pourtant assez simple une fois que l'on a compris les bases.

Les bases
=========

La plupart des terminaux peuvent afficher 256 couleurs ou plus, mais le shell
n'a besoin que de six couleurs de base (auxquelles viennent s'ajouter le blanc
et le noir que je ne traite pas ici). Il s'agit des trois couleurs primaires,
et les trois couleurs secondaires, que l'on peut représenter la palette sous
forme de tableau (dans l'ordre d'index utilisé dans un terminal) :

.. raw:: html

    <table cellspacing="2" cellpadding="3" border="1">
      <tbody>
      <tr>
        <th scope="row">Couleur</th>
        <td width="15%">Rouge</td>
        <td width="15%">Vert</td>
        <td width="15%">Jaune</td>
        <td width="15%">Bleu</td>
        <td width="15%">Magenta</td>
        <td width="15%">Cyan</td>
      </tr>
      <tr>
        <th scope="row">Clair</th>
        <td bgcolor="Red"></td>
        <td bgcolor=#00FF00></td>
        <td bgcolor="Yellow"></td>
        <td bgcolor="Blue"></td>
        <td bgcolor="Fuchsia"></td>
        <td bgcolor="Aqua"></td>
      </tr>
      <tr>
        <th scope="row">Sombre</th>
        <td bgcolor=#800000></td>
        <td bgcolor=#008000></td>
        <td bgcolor=#808000></td>
        <td bgcolor=#000080></td>
        <td bgcolor=#800080></td>
        <td bgcolor=#008080></td>
      </tr>
    </tbody></table>

Cette représentation sous forme de tableau présente l'avantage de montrer deux
caractéristiques : le nombre de couleurs de base est limité, et la variante
sombre ou claire consiste juste à changer la luminosité.

Construire le thème
===================

Nous allons créer deux thèmes : un clair et un sombre.


Pour faire un thème homogène, il va falloir respecter deux règles :

- Chaque couleur devra avoir la même luminosité que les autres couleurs
  du thème : il ne faut pas qu'une couleur éclate davantage que les autres.
- Chaque couleur devra occuper le même espace [#]_ dans le cercle
  colorimétrique : il ne faut pas que deux couleurs semblent proches au point
  de les confondre.

Pour respecter ces deux contraintes, nous allons être obligé de construire
notre thème via une application, qui sera plus à même que nous de respecter ces
deux contraintes.

La représentation des couleurs
==============================

Il existe plusieurs manières de représenter les couleurs en informatique, la
plus connue est la `représentation RGB`_, mais celle-ci n'est pas très pratique
dans notre cas d'utilisation : en effet il n'est pas possible en utilisant
cette représentation de passer facilement de la teinte claire à sombre. Nous
allons nous intéresser ici à la représentation `Teinte/Saturation/Valeur`_ qui
présente l'avantage de séparer la teinte d'une couleur de sa luminosité.

Selon la codification TSL, chaque teinte de couleur est séparée de 60°. Il
faudra donc conserver cet écart dans notre thème final. Par contre nous avons
la possibilité d'appliquer une rotation du cercle pour faire varier la teinte
des couleurs, c'est ce qui donnera les couleurs de notre thème. Il ne nous
reste plus qu'à définir les valeurs de saturation/valeur pour le thème clair et
le thème sombre.

Après tout le reste n'est que conversion d'un système de représentation à un
autre, ce qui peut se faire tout seul avec un petit script.


Le résultat
===========

Le script est écrit en python et peut être `téléchargé ici`_, il reçoit 5
paramètres, et écrit sur la sortie la configuration pouvant être utilisée avec
`rxvt` et `xterm`, deux émulateurs de terminaux permettant un bon niveau de
configuration.


.. _téléchargé ici: {static}/resources/colors.py

.. list-table::
    :header-rows: 1

    * - décalage
      - saturation claire
      - saturation sombre
      - valeur claire
      - valeur sombre
    * - 30
      - 60
      - 50
      - 90
      - 70

Ce qui donnera l'exécution suivante :

.. code-block:: bash

    $ python colors.py 30 60 50 90 70

Il est possible de modifier les couleurs sans enregistrer le paramétrage
définitivement en passant la sortie du script à `xrdb` avant d'ouvrir un
nouveau terminal :

.. code-block:: bash

    $ python colors.py 30 60 50 90 70  | xrdb -override

Si vous voulez appliquer ce paramétrage définitement, il faut l'enregistrer
dans le fichier `~/.Xdefaults`

Le résulat des couleurs générées avec les valeurs définies ci-dessus :

.. raw:: html

    <table cellspacing="2" cellpadding="3" border="1">
      <tbody>
      <tr>
        <th scope="row">Couleur</th>
        <td width="15%">Rouge</td>
        <td width="15%">Vert</td>
        <td width="15%">Jaune</td>
        <td width="15%">Bleu</td>
        <td width="15%">Magenta</td>
        <td width="15%">Cyan</td>
      </tr>
      <tr>
        <th scope="row">Clair</th>
        <td bgcolor=#e6175c></td>
        <td bgcolor=#a1e65c></td>
        <td bgcolor=#e6a15c></td>
        <td bgcolor=#5ca1e6></td>
        <td bgcolor=#a15ce6></td>
        <td bgcolor=#5ce6a1></td>
      </tr>
      <tr>
        <th scope="row">Sombre</th>
        <td bgcolor=#b32c59></td>
        <td bgcolor=#86b359></td>
        <td bgcolor=#b38659></td>
        <td bgcolor=#5986b3></td>
        <td bgcolor=#8659b3></td>
        <td bgcolor=#59b386></td>
      </tr>
    </tbody></table>

Et voilà ce que ça donne en réeel : des couleurs plutôt douces et claires qui
n'agressent pas les yeux ^^

.. image:: {static}/images/Capture-terminal.png
    :class: center
    :alt: caputre d'écran

Vous pouvez à votre tour essayer d'autres combinaisons pour vous créer votre
propre palette à utiliser dans votre terminal.


.. _représentation RGB: https://fr.wikipedia.org/wiki/Rouge_vert_bleu
.. _Teinte/Saturation/Valeur: https://fr.wikipedia.org/wiki/Teinte_Saturation_Valeur

.. [#]

    On peut concevoir l'ensembre des couleurs comme un système, c'est-à-dire un
    ensemble d'éléments interdépendants les uns des autres : l'espace occupé
    par un élément correspond à l'espace qui n'est pas pris par l'ensemble des
    autres éléments.

    Comme les six couleurs de base sont uniformément réparties dans le cercle
    colorimétrique, il faut que notre thème final conserve cette répartition.

