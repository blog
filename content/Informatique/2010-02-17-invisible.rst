.. -*- mode: rst -*-
.. -*- coding: utf-8 -*-

Les utilisateurs invisibles de Linux
####################################

:date: 2010-02-17
:tags: Libre, Humeur
:summary: |summary|

Bonjour à tous, pour mon premier article sur le planet-libre, je voudrais faire
part d'une réflexion qui m'interpelle depuis un moment concernant l'univers
Linux : le fait que les utilisateurs non administrateurs soient exclus de toute
la documentation/prise en main que l'on peut trouver sur le système. Je ne
donne ici que quelques aspects de cette réflexion mais je pense qu'elle touche
l'ensemble des participants au monde du libre.

.. |summary| replace::
    La plupart des articles que l'on peut voir sur le net qui concernent
    l'utilisation du PC sous Linux restent limités à un point : souvent ils
    oublient le fait que plusieurs utilisateurs puissent être enregistrés sur
    le PC, et que tous ne soient pas forcément des administrateurs ( ceux qui
    peuvent avoir des droits root sur la machine). Pourquoi donc ?  Est-ce que
    cela signifie que la plupart des linuxiens sont les seuls à utiliser le PC ?
    C'est possible, mais là n'est pas mon sujet. Je pense que le problème est
    que les utilisateurs sont pour l'instant invisible de la littérature sur
    Linux que l'on peut trouver sur le net. À la fois invisible du côté des
    distributions, et invisible du côté des communautés.

|summary|

Le problème se retrouve présent dans deux aspects : d'une part dans la
documentation s'adressant aux administrateurs, et d'autre part dans la
documentaiton s'adressant aux utilisateurs.

Si l'on suit les manipulations que l'on peut trouver un peu partout sur le net,
on trouve souvent des modifications qui ont pour conséquences de modifier la
configuration générale du système, et l'on trouve plus souvent des
modifications dans /etc/ que dans ~/.config/

Suivre les besoins des utilisateurs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Tous les utilisateurs n'utilisent pas forcément l'ordinateur de la même manière
et il faut prévoir quels sont leurs besoins avant de se lancer dans une
opération générale. Par exemple, il n'y a pas longtemps était paru sur le
planet-libre un article sur privoxy qui se terminait par une manière élégante
d'utiliser privoxy sans configuration supplémentaire\ [1]_. Or privoxy est lent
pour traiter les sites puisant des ressources un peu partout — par exemple
google news ou planet-libre (!) et se transformer en inconfort pour
l'utilisateur.

Les mises à jour
~~~~~~~~~~~~~~~~

Faire une mise à jour est toujours quelque chose de périlleux, et l'on ne sait
pas forcément comment le système va réagir; entre le logiciel qui ne fonctionne
plus car sa configuration a changé ou celui qui ne fonctionne plus car un bug a
été introduit dans la nouvelle version, les risques sont possibles (je n'ai par
exemple pu plus lire de dvd lors de la mise à jour du noyau 2.6.30\ [2]_…)

Je ne veux pas relancer le débat sur le packaging des distributions ( rolling
release contre version fixes) mais le problème doit être posé : comment être
sûr en faisant une mise à jour que l'on ne va pas casser tel composant ?

En plus des modifications générales sur la configuration que peuvent introduire
les modifications, on peut se retrouver dans la situation inverse :
l'utilisateur n'a pas le droit de visualiser les fichiers de logs, d'installer
un paquet ou de modifier un fichier de configuration et ne pourra donc pas
suivre la documentation qu'il peut trouver ici et là sur le net.

Pouvoir utiliser ses propres applications ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les distributions n'ont pour l'instant pas de solutions pour gérer
l'installation de paquets par un utilisateur normal (qui irait s'installer dans
/opt/ par exemple), pouvant être installés sans droit root, et ne pouvant être
exécutés que par l'utilisateur ayant fait son installation.

Utiliser des commandes non root
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En fait, ce système que l'on nous décrit ouvert ne l'est réellement que si l'on
est admin dessus. Pour les autres, la manipulation se limite à bash, python…
Dans la documentation, on trouve même des exemples demandant à l'utilisateur
d'être root alors qu'une commande équivalente peut être lancée par un
utilisateur normal (par exemple $netstat -ie au lieu de #ifconfig)

Ce problème de l'utilisateur non root est pour l'instant contourné (par exemple
en configurant sudo dès l'installation), mais il reste posé, et n'est jamais
attaqué de front.

Le fait que cette situation ne soit jamais évoquée est pour moi significative
de l'utilisation faite de linux aujourd'hui : bien loin du grand public. Nous
sommes tous ici des utilisateurs bidouilleurs, et ne voyons pas forcément une
utilisation quotidienne d'un utilisateur standard. Je ne veux pas en faire une
généralisation sur l'avenir de Linux et une remise en cause nécessaire. Je pose
juste ici un constat sur une situation qui est pour moi, encore trop souvent
invisible.

.. admonition:: notes :


    .. [1] `Artisan Numérique » Se prémunir des "SpyWebs" avec Privoxy <http://artisan.karma-lab.net/node/1204>`_

    .. [2] `http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=557340 <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=557340>`_
