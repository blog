.. -*- rst -*-
.. -*-  coding: utf-8 -*-

===========================
De la dynamique des projets
===========================

:date: 2015-03-21
:tags: Jeux, DIY
:summary: |summary|
:logo: /images/desire.jpg

.. figure:: {static}/images/desire.jpg
    :figwidth: 150
    :figclass: floatright
    :alt: Goban

    Image : `Wendy`_ (creativecommons_)

.. _Wendy: https://www.flickr.com/photos/smkybear/2395845001
.. _creativecommons: http://creativecommons.org/licenses/by-sa/2.0/

|summary|

.. |summary| replace::
    Il suffit parfois de pas grand-chose, par exemple changer de téléphone,
    pour avoir envie de créer. C'est parti d'un manque — une application qui me
    manquait sur mon nouveau téléphone, et aussi l'envie de faire quelque chose
    par soi-même, plutôt que simplement télécharger un outil tout prêt qu'il
    suffit d'installer.

Toujours est-il que je me suis mis à coder pour mon téléphone. Pour m'amuser,
pour apprendre, pour m'occuper dans le train une fois que j'aurai terminé mon
jeu…

S'amuser
--------

Le moteur le plus important, sûrement. Prendre du plaisir dans ce que l'on
fait. Déjà le jeu. Réfléchir, se prendre la tête sur un problème, chercher la
solution, tâtonner, revenir en arrière et recommencer. J'ai toujours aimé
trouver des réponses à des problèmes. Ou du moins la chercher. Une fois que
l'on a compris comment trouver la réponse, l'enjeu disparaît, même si la
solution n'a pas été exprimée jusqu'au bout…

Ici le sujet est bien choisi : des problèmes de go. Il faut trouver la
solution, comprendre le problème et ce que l'on attend de nous. Survivre —
trouver une défense au prix de la perte de quelques pierres ? Attaquer —
trouver la faille dans le jeu de l'adversaire ?

Identifier un motif pour comprendre comment répondre a un besoin en s'insérant
dans une configuration existante, c'est un jeu de puzzle du quotidien. La même
problématique se pose dans le cadre de la programmation ; surtout quand il
s'agit de mener un projet seul, dans lequel il est nécessaire de construire
l'ensemble des fonctions d'une application en gardant à l'esprit comment
l'application va évoluer à l'avenir.

Apprendre
---------

Vient ensuite un autre plaisir, celui d'apprendre. Être confronté à une
nouvelle situation devant laquelle on commence sans savoir quoi faire. Le
téléphone nécessite son propre environnement de travail (même si des
simulateurs existent). Les concepteurs ont choisi des technologies existantes,
mais encore faut-il les connaître, ce qui n'était pas mon cas quand j'ai
commencé à développer mon jeu.

Il y a donc un besoin d'apprendre de nouvelles connaissances, mais surtout, une
dynamique de l'apprentissage. Cette dynamique qui me pousse sans cesse à aller
*au fond de l'Inconnu pour trouver du nouveau !*

Persévérer
----------

.. figure:: {static}/images/time/1135634742_0b4735dab5_m.jpg
    :figwidth: 182
    :figclass: floatleft
    :alt: Perséphone

    Image : `Olga Diez`_ (creativecommons__)

.. _by-nc-sa: http://creativecommons.org/licenses/by-nc-sa/2.0/

__ by-nc-sa_

.. _Olga Diez: https://www.flickr.com/photos/caliope-olga/1135634742/

Ensuite viennent les obstacles : Ne pas savoir faire, être bloqué sur un
problème que l'on ne comprend pas, perdre du temps à chercher. *Bien qu'on ait
du cœur à l'ouvrage, l'art est long, et le temps est court* dit le poète.
Pendant six mois, le projet est resté endormi sans que je ne cherche à m'y
remettre. Souvent, cela commence par un problème que je n'avais pas imaginé.
Un obstacle un peu plus difficile que les autres, qui oblige à prendre une
heure de réflexion avant de le résoudre. Sauf que c'est long une heure (moins
long que six mois d'attente certes)…

Quand la seule récompense à cette motivation est une satisfaction personnelle,
il est facile de laisser tomber. Passer à une autre activité dont le plaisir et
la récompense est immédiate. Combien de projets ai-je déjà laissé sombrer,
avant qu'une nouvelle idée arrive, et qu'un nouveau projet vienne chasser le
précédent.

Ici la dynamique est repartie, venant cette fois d'un autre jeu ; une
découverte, m'a redonné l'envie. L'envie de jouer, de créer, d'expérimenter.
C'est le balancier de Perséphone qui oscille, entre la phase de création et
celle de son repos.

Diffusion
---------

Le projet a été diffusé cette semaine. C'est une première version, mais c'est
une première tout court. La naissance d'un nouveau projet qui va venir
compléter tous ceux pour lesquels des créateurs ont donné de leur temps.
Maintenant il reste à continuer à le faire vivre, et ne pas le laisser mourir !
