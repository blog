.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

=========================================
Cachez-moi ce menu que je ne saurais voir
=========================================

:date: 18/05/2013
:tags: Humeur
:summary: |summary|
:logo: {static}/images/traitement/logo.jpg
:status: hidden


.. figure:: https://farm1.staticflickr.com/214/4555895229_880a76beb7_q.jpg
    :figclass: floatright
    :alt: Machine ancienne

    Photo : `Arielle Fragassi`_

Dans la programmation, la maintenance du code, c'est-à-dire la réutilisabilité
d'une partie de l'application par un tiers ou l'utilisation de ce code dans un
autre contexte que celui pour lequel il a été écrit est tout aussi important
que le bon fonctionnement de l'application.


Comment des développeurs peuvent-ils concevoir des applications qui incitent les
utilisateurs à produire des documents qui ne soient pas maintenables ?

Ma question s'adresse aux logiciels de traitement de texte, et cible cette
ignominie héritée des suites bureautiques d'un ancien temps : je ne comprends
pas pourquoi libreOffice continue de présenter une barre de formatage de texte
incluant les boutons pour choisir la police sa taille et appliquer  des effets
de style sur le texte.

|summary|

.. |summary| replace::
    Si je devais écrire un logiciel de traitement de texte, j'irais cacher ces
    boutons dans un obscur menu, et enverrai des décharges électriques à
    l'utilisateur à chaque fois qu'il prend le risque de les utiliser malgré
    tout !  Pourquoi ? Parce que ces boutons sont utilisés la plupart à tort et
    à travers, et compliquent la tâche des usagers plus qu'ils la facilitent.

.. image:: {static}../images/traitement/fautif.png
    :alt: Le menu coupable

Le nombre de fois où il est légitime d'utiliser ces boutons ne justifient au
aucun cas leur présence en pleine page, juste au-dessus du document sur lequel
on est en train de travailler !

Maikeskidi ?
============

Trop souvent je vois des documents dont le formatage repose seulement sur des
styles appliqués via ce menu : les titres, les paragraphes, les effets de mise
en page étant uniquement géré via des mises en gras, des changements de police,
et, comble de l'horreur, des espaces pour gérer les retraits !

Comme je plains l'auteur d'un document, qui, trois mois après avoir écrit son
CV, essaie de rajouter une ligne dans ses expériences ; lorsque son beau
formatage se retrouve cassé parce que sa nouvelle ligne est plus longue que les
précédentes ! Comme je plains l'utilisateur qui est obligé de re-numéroter tous
ses titres parce qu'une nouvelle section a été insérée sur la 4e page de son
rapport !

Et pourtant, je comprends pourquoi les documents sont ainsi rédigés : c'est
tellement intuitif, tellement facile de mettre en forme son document
directement, en appliquant le formatage sur le texte sélectionné. Après tout,
on utilise un traitement de texte avoir un rendu visuel immédiat du document,
pas pour passer son temps dans les styles, à mettre en place une mise en forme
abstraite, nommée « Titre2 » juste pour configurer un sous-titre que l'on
utilise une seule fois dans son texte ! Mais lorsque le rapport se met à
grossir, une fois ces sous-titres plus nombreux, quand on souhaitera mettre les
sous-titre en italique plutôt que les souligner, le temps passé à mettre en
forme le document causera énervement, erreurs, et frustration.

Pourtant, le retirer signifie couper les utilisateurs de leurs habitudes et les
placer face à une application qu'ils maîtriserons encore moins. Entre ces deux
solutions extremes (encourager l'utilisateur utiliser incorrectement le
produit, ou le contraindre à l'utiliser tel qu'il a été pensé par les
développeurs), il y a une marge : accompagner l'utilisateur dans son usage de
l'application.

On fait quoi alors ?
====================

Pour l'utilisateur qui ne sait pas : `utiliser les styles`_ ! Je ne vais pas
faire un tutoriel sur leur usage, mais il s'agit de la solution la plus simple
pour mettre en forme un document.

Ensuite, pour la communauté, alerter : c'est en remontant l'information
et en échangeant que les logiciels évoluent ; pas en pestant tout seul dans son
coin. Si l'on ne prend pas le temps de remonter un problème (comme ici à
travers cet article) rien n'évoluera, et la situation persistera. Cette alerte
peut se faire directement via des remontées de bugs, des réunions
d'information, ou des commentaires dans forums. Le but est de parler, et
échanger sur ce que l'on perçoit.

Comment contribuer ?
====================

Enfin pour le développeur, être à l'écoute. C'est une faute grave de ne pas
suivre les remontées de ces utilisateurs, surtout quand un problème est connu
(les articles qui présentent l'utilisation des styles sont connus et existent
depuis longtemps, preuve que le besoin de documentation n'est pas nouveau).
Mettre en place une pop-up affichant « Êtes vous sûr de vouloir appliquer un
effet de texte local ? » peut aussi être une solution.

Malheureusement, plus le projet devient imposant, plus contribuer devient
difficile. Tout d'abord parce qu'avant de proposer un patch, il est nécessaire
de prendre le temps et se plonger dans le code existant (ce qui casse le patch
spontané corrigeant un problème local), mais aussi parce que plus un projet
devient imposant, plus il acquiert une inertie qui l'empêche d'évoluer
facilement.

.. _Utiliser les styles: https://help.libreoffice.org/Writer/Styles_in_Writer/fr


.. _Arielle Fragassi: http://www.flickr.com/photos/toastytreat/4555895229/
