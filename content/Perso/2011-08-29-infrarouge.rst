.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-

Photo infrarouge
################

:date: 2011-08-29

Je me suis récemment essayé à la photo infrarouge; en mettant un filtre qui ne laisse
passer que les rayons infrarouges devant l'objectif, il est possible de saisir des images
que l'on ne verrait pas à l'œil nu.

Ce n'est plus les couleurs qui sont saisies, mais la température : généralement l'eau et
le ciel seront sombre, alors que les feuilles et l'herbe seront très clairs.

Alors que les beaux jours reviennent, voici la photo d'un jardin d'enfants à midi :

.. image:: {static}/images/2011avril23_120334.jpg
    :width: 650
    :alt:   photo infrarouge
