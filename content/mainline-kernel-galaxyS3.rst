:title: Getting the mainline kernel working on the Galaxy S3
:date: 2016-11-19 19:54
:tags: kernel, free software, sustainability, phone
:category: General
:author: Wolfgang Wiedmeyer
:status: draft
:summary: challenges with mainline kernel, initial work

In a `previous post <{filename}/newer-kernel-galaxyS3.rst>`_, I already wrote about the issues getting a recent kernel working on a Galaxy S3, specifically about merging certain parts or everything of the current old kernel for the Galaxy S3 with a more recent kernel. It turns out trying to get the mainline kernel working is the only viable solution in the long-term. If you want to update the kernel of your device, you don't want to do this once but every time you think it's worth it. So you need a solution that is maintainable and makes kernel updates as painless as possible. To make this work, you basically need as much support as possible for your device in the mainline kernel so your kernel differs as little as possible from the mainline kernel. Then you only have port a little set of patches to a newer kernel release.

Unfortunately, there are not many examples where smartphone developers went all the way to get almost complete support in the mainline kernel and where the smartphone was actually maintained across several major kernel releases. The `GTA04 <http://projects.goldelico.com/p/gta04-main/>`_ is the only phone among these examples that is still maintained, as far as I'm informed. It will be the base for the `Neo900 <https://neo900.org/>`_. Paul Kocialkowski is working on getting mainline kernel support for the `Optimus Black <http://code.paulk.fr/article23/a-hacker-s-journey-freeing-a-phone-from-the-ground-up-fourth-part>`_.

But what about the Galaxy S3? Compared to most other phones, it turns out to actually be a really good choice in regards to mainline kernel support. Samsung re-released it for `Tizen <https://en.wikipedia.org/wiki/Tizen>`_. While their Galaxy S3 kernel for Android was based on the `3.0.31 <{filename}/newer-kernel-galaxyS3.rst#kernel-version-3-0-make-that-old>`_ Linux kernel, 

not important how new kernel, only important how much support in mainline

Grsecurity

Nexus 7

GTA04

Optimus Black

UART
####
