window.addEventListener("load", async () => {
  try {
    await Scheme.load_main("propagators.wasm", {
      reflect_wasm_dir: "hoot",
      user_imports: {
        window: {
          setTimeout: setTimeout
        },
        document: {
          makeTextNode: Document.prototype.createTextNode.bind(document),
          makeElement: Document.prototype.createElement.bind(document),
          body: () => document.body,
        },
        element: {
          appendChild: (parent, child) => parent.appendChild(child),
          setAttribute: (elem, attr, value) => elem.setAttribute(attr, value),
          getValue: (elem) => elem.value,
          setValue: (elem, val) => elem.value = val,
          replaceWith: (oldElem, newElem) => oldElem.replaceWith(newElem),
          addEventListener: (elem, name, f) => elem.addEventListener(name, f)
        },
      }
    });
  } catch(e) {
    if(e instanceof WebAssembly.CompileError) {
      document.getElementById("wasm-error").hidden = false;
    }
    throw e;
  }
});
